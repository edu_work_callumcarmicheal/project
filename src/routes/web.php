<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', 'HomeController@test')->name('testPage');


Auth::routes();

// Timetables
Route::get('/timetable/{timetable}', 'TimetableController@viewTable');
Route::get('/req/timetable/{timetable}/table', 'TimetableController@generateTableJson');
Route::get('/timetable', 'TimetableController@listTables')->name('timetables');

Route::group(['middleware' => ['auth']], function() {
	// Home controller
	Route::get('/home', 'HomeController@index')->name('home');

	// Requests
	Route::post('/req/resource-collection',          'ResourceController@retrieveCollectionList');
	Route::post('/req/resources/{rc}/staff',         'ResourceController@retrieveStaffList');
	Route::post('/req/resources/{rc}/buildings',     'ResourceController@retrieveBuildingList');
	Route::post('/req/resources/buildings/{b}/rooms', 'ResourceController@retrieveRoomList');

	Route::post('/req/slot/{slot}/update',           'ResourceController@updateSlot');

	// CRUD
	//Route::group
});
