<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimetablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timetables', function (Blueprint $table) {
	        $table->increments('id');
	
	        $table->string("name");
	        $table->integer('created_by')->unsigned()->nullable();
			$table->text("description")->default("");
	
	        $table->enum('visibility_state', ['private', 'public_auth', 'public_guest'])->default('private');
	        $table->string('password')->nullable();
	
	        $table->dateTime("start_date");
	        $table->dateTime("end_date");
	
	        $table->timestamps();
	
	        /* Relationships */
	        $table
		        ->foreign('created_by')
		        ->references('id')
		        ->on('users')
		        ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timetables');
    }
}
