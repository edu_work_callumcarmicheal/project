<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
	        $table->increments('id');

	        $table->integer("timetable_id")->unsigned();
	        $table->string("identifier");
	        $table->string("name")->nullable()->default(null); // TODO: Document

	        $table->date("start_date")->nullable();
	        $table->date("end_date")->nullable();

	        $table->integer("created_by")->unsigned();

	        $table->timestamps();

	        /* Relationships */
	        $table
		        ->foreign('timetable_id')
		        ->references('id')
		        ->on('timetables')
		        ->onDelete('cascade');
	        $table
		        ->foreign('created_by')
		        ->references('id')
		        ->on('users')
		        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
