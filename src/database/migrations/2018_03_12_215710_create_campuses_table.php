<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campuses', function (Blueprint $table) {
	        $table->increments('id');

	        $table->integer("address_id")->unsigned();
	        $table->string("name");

	        $table->integer("created_by")->unsigned();

	        $table->timestamps();

	        /* Relationships */
	        $table
		        ->foreign('address_id')
		        ->references('id')
		        ->on('addresses')
		        ->onDelete('cascade');
	        $table
		        ->foreign('created_by')
		        ->references('id')
		        ->on('users')
		        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campuses');
    }
}
