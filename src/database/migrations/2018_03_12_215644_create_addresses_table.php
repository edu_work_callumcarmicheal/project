<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');

	        $table->string("address");
	        $table->string("postcode");

	        // DOCME: Add to ERD
	        $table->integer("created_by")->unsigned();
	        $table->integer('resource_id')->unsigned();

	        $table->timestamps();

	        /* Relationships */
	        $table
		        ->foreign('created_by')
		        ->references('id')
		        ->on('users')
		        ->onDelete('cascade');
	        $table
		        ->foreign('resource_id')
		        ->references('id')
		        ->on('resource_collections')
		        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
