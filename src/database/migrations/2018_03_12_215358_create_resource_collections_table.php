<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourceCollectionsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('resource_collections', function (Blueprint $table) {
			$table->increments('id');
			
			$table->string("name");
			$table->integer("created_by")->unsigned();
			$table->enum("visibility", ['private', 'public_rw', 'public_ro'])->default('private');
			
			$table->timestamps();
			
		    /* Relationships */
			$table
				->foreign('created_by')
				->references('id')
				->on('users')
				->onDelete('cascade');
		});
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('resource_collections');
	}
}
