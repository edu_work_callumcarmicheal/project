<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOccurrencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('occurrences', function (Blueprint $table) {
	        $table->increments('id');

	        $table->integer("unit_id")->unsigned();
	        $table->integer("occurrence_number");
	        $table->string("label")->nullable();

	        // DOCME: Add to ERD
	        $table->integer("created_by")->unsigned();

	        $table->timestamps();

	        /* relationships */
	        $table
		        ->foreign('unit_id')
		        ->references('id')
		        ->on('units')
		        ->ondelete('cascade');
	        $table
		        ->foreign('created_by')
		        ->references('id')
		        ->on('users')
		        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('occurrences');
    }
}
