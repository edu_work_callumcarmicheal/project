<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimetableSharedUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timetable_shared_users', function (Blueprint $table) {
            $table->increments('id');
            
	        $table->integer("timetable_id")->unsigned();
	        $table->integer("user_id")->unsigned();
	        $table->integer("created_by")->unsigned();
	        $table->enum('access_level', ['wr', 'ro'])->default('ro');
			
	        $table->timestamps();
			
	        /* Relationships */
	        $table
		        ->foreign('timetable_id')
		        ->references('id')
		        ->on('timetables')
		        ->onDelete('cascade');
	        $table
		        ->foreign('user_id')
		        ->references('id')
		        ->on('users')
		        ->onDelete('cascade');
	        $table
		        ->foreign('created_by')
		        ->references('id')
		        ->on('users')
		        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timetable_shared_users');
    }
}
