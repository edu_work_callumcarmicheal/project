<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slots', function (Blueprint $table) {
	        $table->increments('id');

	        $table->integer("course_id")->unsigned();
	        $table->integer("room_id")->unsigned();
	        $table->integer("staff_id")->nullable()->unsigned();
	        $table->integer("occurrence_id")->unsigned();

	        $table->integer("day");
	        $table->time("start_time");
	        $table->time("end_time");

	        $table->integer("created_by")->unsigned();

	        $table->timestamps();

	        /* Relationships */
	        $table
		        ->foreign('course_id')
		        ->references('id')
		        ->on('courses')
		        ->onDelete('restrict');
	        $table
		        ->foreign('room_id')
		        ->references('id')
		        ->on('rooms')
		        ->onDelete('restrict');
	        $table
		        ->foreign('staff_id')
		        ->references('id')
		        ->on('staff')
		        ->onDelete('restrict');
	        $table
		        ->foreign('occurrence_id')
		        ->references('id')
		        ->on('occurrences')
		        ->onDelete('restrict');
	        $table
		        ->foreign('created_by')
		        ->references('id')
		        ->on('users')
		        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('slots');
    }
}
