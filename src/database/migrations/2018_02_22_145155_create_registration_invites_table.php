<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistrationInvitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('registration_invites', function (Blueprint $table) {
		    $table->increments('id');

		    $table->dateTime("expires_on");
		    $table->integer('accepted_by')->unsigned()->nullable();

		    $table->dateTime("accepted_at")->nullable();

		    $table->string("email")->index();
		    $table->string("name");
		    $table->string("token")->index();

		    $table->integer("created_by")->unsigned()->nullable();
		    $table->integer("systemrole_id")->unsigned()->nullable();

		    $table->timestamps();

		    /* Relationships */
		    $table
			    ->foreign('accepted_by')
			    ->references('id')
			    ->on('users')
			    ->onDelete('cascade');
		    $table
			    ->foreign('created_by')
			    ->references('id')
			    ->on('users')
			    ->onDelete('set null'); //*/
		    $table
			    ->foreign('systemrole_id')
			    ->references('id')
			    ->on('system_roles')
			    ->onDelete('set null');
	    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registration_invites');
    }
}
