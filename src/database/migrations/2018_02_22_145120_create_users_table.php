<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
	        $table->increments('id');
	        $table->string('name');
	        $table->string('email')->unique();
	        $table->string('password');
			$table->boolean('enabled')->default(false); // DOCME: Add to the ERD and Data tables
	        
	        $table->integer('systemrole_id')->unsigned()->nullable();

	        $table->rememberToken();
	        $table->timestamps();

	        /* Relationships */
	        $table
		        ->foreign('systemrole_id')
		        ->references('id')
		        ->on('system_roles')
		        ->onDelete('set null'); //*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
