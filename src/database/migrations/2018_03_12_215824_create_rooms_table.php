<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
	        $table->increments('id');

	        $table->integer("building_id")->unsigned();
	        $table->string("room_number");
	        $table->string("name")->nullable();

	        $table->integer("created_by")->unsigned();

	        $table->timestamps();

	        /* Relationships */
	        $table
		        ->foreign('building_id')
		        ->references('id')
		        ->on('buildings')
		        ->onDelete('cascade');
	        $table
		        ->foreign('created_by')
		        ->references('id')
		        ->on('users')
		        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
