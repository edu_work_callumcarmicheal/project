<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuildingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buildings', function (Blueprint $table) {
	        $table->increments('id');

	        $table->integer("campus_id")->unsigned();
	        $table->integer("address_id")->unsigned();
	        $table->string("display");
	        $table->string("name");

	        $table->integer("created_by")->unsigned();

	        $table->timestamps();

	        /* Relationships */
	        $table
		        ->foreign('campus_id')
		        ->references('id')
		        ->on('campuses')
		        ->onDelete('cascade');
	        $table
		        ->foreign('address_id')
		        ->references('id')
		        ->on('addresses')
		        ->onDelete('cascade');
	        $table
		        ->foreign('created_by')
		        ->references('id')
		        ->on('users')
		        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buildings');
    }
}
