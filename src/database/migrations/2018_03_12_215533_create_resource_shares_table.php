<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourceSharesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resource_shares', function (Blueprint $table) {
            $table->increments('id');
	
	        $table->integer("resource_id")->unsigned();
	        $table->integer("created_by")->unsigned();
	        $table->integer("target")->unsigned();
	        $table->enum("type", ['rw', 'ro'])->default('ro');
            
            $table->timestamps();
	
	        /* Relationships */
	        $table
		        ->foreign('resource_id')
		        ->references('id')
		        ->on('resource_collections')
		        ->onDelete('cascade');
	        $table
		        ->foreign('created_by')
		        ->references('id')
		        ->on('users')
		        ->onDelete('cascade');
	        $table
		        ->foreign('target')
		        ->references('id')
		        ->on('users')
		        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resource_shares');
    }
}
