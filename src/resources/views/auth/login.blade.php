<?php
use App\Models\Extensions\WebsiteSettings;

// Get the registration state
$registrationEnabled = WebsiteSettings::settings()->registrationEnabled(); ?>

@extends('layouts.app')
@section('template/title', 'Login')

@section('content')
    <section class="jumbotron text-center" style="background: #fff; margin-bottom: 0px;">
        <div class="container">
            <h1 class="jumbotron-heading">Timetable</h1>
            <p class="lead">please login to view your timetable or manage timetables or export them.</p>
            <p>
                {{-- Show registration button --}}
				@if ($registrationEnabled) <a href="{{ route('register') }}" class="btn btn-primary">Register</a> @endif

                <a href="#" class="btn btn-info">View timetable's</a>
            </p>
        </div>
    </section>

    <div class="container">
	    @if ( session()->has('message') )
	    <section class="alert alert-danger alert-dismissible">
		    {{ session()->get('message') }}
	    </section>
	    @endif

        <form class="form-horizontal" method="POST" action="{{ route('login') }}" style="max-width:600px; margin: 0 auto;">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="control-label">E-Mail Address</label>

                <div class="">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="control-label">Password</label>

                <div class="">
                    <input id="password" type="password" class="form-control" name="password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                    <button type="submit" class="btn btn-primary">
                        Login
                    </button>

                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        Forgot Your Password?
                    </a>
                </div>
            </div>
        </form>
    </div>
    <!-- /container -->
@endsection