@extends('layouts.app')
@section('template/title', 'Register')

@section('content')
    <section class="jumbotron text-center" style="background: #fff; margin-bottom: 0px;">
        <div class="container">
            <h1 class="jumbotron-heading">Timetable</h1>
            <p class="lead">please login to view your timetable or manage timetables or export them.</p>
            <p>
                {{-- Show login button --}}
                <a href="{{ route('login') }}" class="btn btn-primary">Login</a>

                <a href="#" class="btn btn-info">View timetable's</a>
            </p>
        </div>
    </section>

    <div class="container">
        <form class="form-horizontal" method="POST" action="{{ route('register') }}" style="max-width:600px; margin: 0 auto;">
            {{ csrf_field() }}

            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="control-label">Name</label>

                <div class="">
                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                    @if ($errors->has('name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class="control-label">E-Mail Address</label>

                <div class="">
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="control-label">Password</label>

                <div class="">
                    <input id="password" type="password" class="form-control" name="password" required>

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <label for="password-confirm" class="control-label">Confirm Password</label>

                <div class="">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                </div>
            </div>

            <div class="form-group">
                <div class="">
                    <button type="submit" class="btn btn-primary">
                        Register
                    </button>
                </div>
            </div>
        </form>
    </div>
    <!-- /container -->
@endsection