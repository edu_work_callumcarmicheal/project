@extends('layouts.app')

@section('content')
<div class="container">
    @if ( session()->has('message') )
        <div class="row">
            <section class="alert alert-danger alert-dismissible">
                {{ session()->get('message') }}
            </section>
        </div>
    @endif

    <section class="jumbotron text-center" style="background: #fff">
        <div class="container">
            <h1 class="jumbotron-heading">Timetable</h1>
            <p class="lead">Welcome {{auth()->user()->name}}, to get started select a option below.</p>
            <p>
                <a href="{{ route('timetables') }}" class="btn btn-primary">View Timetables</a>
            </p>
        </div>
    </section>
</div>
@endsection
