<?php
/**
 * Proj: PhpStorm
 * User: CallumCarmicheal
 * Date: 18/02/2018
 * Time: 20:43
 */ ?>

@extends('layouts.app')

@section('content')
	<!-- Breadcrumbs -->
	<div class="header-breadcrumbs">
		<div class="container">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Home</a></li>
				<li class="breadcrumb-item"><a href="#">LANG-17-18</a></li>
				<li class="breadcrumb-item active">Manage</li>
			</ol>
		</div>
	</div>

	<!-- Page Container / App -->
	<div class="app container" id="app-body">
		<!-- Page Header -->
		<div class="page-header" id="banner">
			<div class="row">
				<div class="col-sm-12 col-md-12 col-lg-12">
					<h1>LANG-17-18</h1>
					<p class="lead">Timetable Description</p>
				</div>
				<div class="col-lg-4 col-md-5 col-sm-6">Timetable details</div>
			</div>
		</div>

		<div style="height:20px;"></div>

		<!-- Button trigger modal -->
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#mdl-Navigation">
			Launch navigation test
		</button>

		<div style="height:20px;"></div>

		<div class="timetable vert1 m-b-110" data-display-type="desktop" data-type="ElementController" data-controller="TimetableController">
			<table data-vertable="ver1">
				<thead>
					<tr class="timetable-row head">
						<th class="timetable-col column1" data-column="column1">COURSE</th>
						<th class="timetable-col column2" data-column="column2">DAY</th>
						<th class="timetable-col column3" data-column="column3">START</th>
						<th class="timetable-col column4" data-column="column4">END</th>
						<th class="timetable-col column5" data-column="column5">ROOM</th>
						<th class="timetable-col column6" data-column="column6">STAFF</th>
						<th class="timetable-col column7" data-column="column7">CODE</th>
						<th class="timetable-col column8" data-column="column8">SUBJECT</th>
					</tr>
				</thead>
				<tbody>

                @for($x = 0; $x < 3; $x++)
                    <tr class="timetable-row">
                        <td rowspan="5" data-column="column1" class="timetable-col column1">

                            <div>
                                <div class="text-center">HNDCOMSOFT-TEST2</div>
                                <div class="row">
                                    <div class="col-md-6 text-left">05-06-18</div>
                                    <div class="col-md-6 text-right">07-12-18</div>
                                </div>
                            </div>

                        </td>
                        <td class="timetable-col column2" rowspan="2" data-column="column2">Monday</td>
                        <td class="timetable-col column3" data-column="column3">09:00</td>
                        <td class="timetable-col column4" data-column="column4">12:00</td>
                        <td class="timetable-col column5" data-column="column5">MS-12</td>
                        <td class="timetable-col column6" data-column="column6">Griffiths, Michael</td>
                        <td class="timetable-col column7" data-column="column7">H17L34/020</td>
                        <td class="timetable-col column8" data-column="column8">Human Computer Interaction</td>
                    </tr>

                    <tr class="timetable-row">
                        <td class="timetable-col column3" data-column="column3">09:00</td>
                        <td class="timetable-col column4" data-column="column4">12:00</td>
                        <td class="timetable-col column5" data-column="column5">MS-12</td>
                        <td class="timetable-col column6" data-column="column6">Griffiths, Michael</td>
                        <td class="timetable-col column7" data-column="column7">H17L34/020</td>
                        <td class="timetable-col column8" data-column="column8">Human Computer Interaction</td>
                    </tr>


                    <!-- Seperator -->
                    <tr><td class="subject-separator" colspan="7"></td></tr>

                    <tr class="timetable-row">
                        <td class="timetable-col column2" rowspan="2" data-column="column2">Tuesday</td>
                        <td class="timetable-col column3" data-column="column3">09:00</td>
                        <td class="timetable-col column4" data-column="column4">12:00</td>
                        <td class="timetable-col column5" data-column="column5">MS-12</td>
                        <td class="timetable-col column6" data-column="column6">Griffiths, Michael</td>
                        <td class="timetable-col column7" data-column="column7">H17L34/020</td>
                        <td class="timetable-col column8" data-column="column8">Human Computer Interaction</td>
                    </tr>
                    <tr class="timetable-row">
                        <td class="timetable-col column3" data-column="column3">09:00</td>
                        <td class="timetable-col column4" data-column="column4">12:00</td>
                        <td class="timetable-col column5" data-column="column5">MS-12</td>
                        <td class="timetable-col column6" data-column="column6">Griffiths, Michael</td>
                        <td class="timetable-col column7" data-column="column7">H17L34/020</td>
                        <td class="timetable-col column8" data-column="column8">Human Computer Interaction</td>
                    </tr>

                    <!-- Seperator -->
                    <tr><td class="course-separator" colspan="8"></td></tr>

                    <tr class="timetable-row">
                        <td rowspan="5" data-column="column1" class="timetable-col column1">

                            <div>
                                <div class="text-center">"Test Course"</div>
                                <div class="text-center">HNDCOMSOFT-TEST2</div>
                                <div class="row">
                                    <div class="col-md-6 text-left">05-06-18</div>
                                    <div class="col-md-6 text-right">07-12-18</div>
                                </div>
                            </div>

                        </td>
                        <td class="timetable-col column2" rowspan="2" data-column="column2">Monday</td>
                        <td class="timetable-col column3" data-column="column3">09:00</td>
                        <td class="timetable-col column4" data-column="column4">12:00</td>
                        <td class="timetable-col column5" data-column="column5">MS-12</td>
                        <td class="timetable-col column6" data-column="column6">Griffiths, Michael</td>
                        <td class="timetable-col column7" data-column="column7">H17L34/020</td>
                        <td class="timetable-col column8" data-column="column8">Human Computer Interaction</td>
                    </tr>

                    <tr class="timetable-row">
                        <td class="timetable-col column3" data-column="column3">09:00</td>
                        <td class="timetable-col column4" data-column="column4">12:00</td>
                        <td class="timetable-col column5" data-column="column5">MS-12</td>
                        <td class="timetable-col column6" data-column="column6">Griffiths, Michael</td>
                        <td class="timetable-col column7" data-column="column7">H17L34/020</td>
                        <td class="timetable-col column8" data-column="column8">Human Computer Interaction</td>
                    </tr>


                    <!-- Seperator -->
                    <tr><td class="subject-separator" colspan="7"></td></tr>

                    <tr class="timetable-row">
                        <td class="timetable-col column2" rowspan="2" data-column="column2">Tuesday</td>
                        <td class="timetable-col column3" data-column="column3">09:00</td>
                        <td class="timetable-col column4" data-column="column4">12:00</td>
                        <td class="timetable-col column5" data-column="column5">MS-12</td>
                        <td class="timetable-col column6" data-column="column6">Griffiths, Michael</td>
                        <td class="timetable-col column7" data-column="column7">H17L34/020</td>
                        <td class="timetable-col column8" data-column="column8">Human Computer Interaction</td>
                    </tr>
                    <tr class="timetable-row">
                        <td class="timetable-col column3" data-column="column3">09:00</td>
                        <td class="timetable-col column4" data-column="column4">12:00</td>
                        <td class="timetable-col column5" data-column="column5">MS-12</td>
                        <td class="timetable-col column6" data-column="column6">Griffiths, Michael</td>
                        <td class="timetable-col column7" data-column="column7">H17L34/020</td>
                        <td class="timetable-col column8" data-column="column8">Human Computer Interaction</td>
                    </tr>

                    <!-- Seperator -->
                    <tr><td class="course-separator" colspan="8"></td></tr>
                @endfor
				</tbody>
			</table>

		</div>
	</div>

@endsection

@section('styles')
	<style>
		.subject-separator {
			height: 5px;
		}

		.course-separator {
			height: 15px;
			/*box-shadow: 0 8px 20px -6px grey;*/
		}

		.after-course-separator {
			height: 15px;
		}

		.timetable-col {
			border: 1px solid #ddd;
		}

		.timetable-col.column2 {
			width: 90px;
		}

		.timetable-col.column3,
		.timetable-col.column4 {
			width: 80px;
		}
	</style>
@endsection

@section('scripts')
	<script>
		$(function() {
			let app = $("#app");
			var offset = $( ".sticky-header" ).offset();
			var sticky = $( ".timetable-row.head" );

			console.log("offset: ", offset);
			console.log("sticky: ", sticky);

			app.scroll(function() {
				let scrollTop = app.scrollTop();
				console.log(scrollTop, offset);

				if ( scrollTop > offset.top){
					$('.sticky-header').addClass('fixed');

					console.log("FIXED!");
				} else {
					$('.sticky-header').removeClass('fixed');
					console.log("NOT FIXED!!");
				}

			});
		});
		(function ($) {
			"use strict";
			var $tt_col = $('.timetable-col');
			$tt_col.on('mouseover', function () {
				var table1 = $(this).parent().parent().parent();
				var table2 = $(this).parent().parent();
				var row = $(this).parent();
				var column = $(this).data('column') + "";
				$(table2).find("." + column).addClass('hov-column');
				$(table1).find(".timetable-row.head ." + column).addClass('hov-column-head');
			});
			$tt_col.on('mouseout', function () {
				var table1 = $(this).parent().parent().parent();
				var table2 = $(this).parent().parent();
				var row = $(this).parent();
				var column = $(this).data('column') + "";
				$(table2).find("." + column).removeClass('hov-column');
				$(table1).find(".timetable-row.head ." + column).removeClass('hov-column-head');
			});
		})(jQuery);
	</script>
@endsection
