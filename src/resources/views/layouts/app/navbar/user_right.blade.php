<?php
/**
 * Created by PhpStorm.
 * User: callu
 * Date: 20/02/2018
 * Time: 10:16
 */

// PLANNED: Navbar Highlighting
?>

@permission('admin')
<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarAdministration" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        Admin
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarAdministration">
        @permission('admin/crud') <a class="dropdown-item" href="#"> CRUD </a> @endpermission
        @permission('admin/timetables') <a class="dropdown-item" href="#"> Manage Timetables </a> @endpermission
        @permission('admin/users') <a class="dropdown-item" href="#"> Manage Users </a> @endpermission
        @permission('admin/roles') <a class="dropdown-item" href="#"> Manage Roles </a> @endpermission
    </div>
</li>
@endpermission

<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" href="#" id="navbarUserSettings" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{ Auth::user()->name }}
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarUserSettings">
        <!-- <a class="dropdown-item" href="#">Action</a>
		<div class="dropdown-divider"></div> -->

        <a class="dropdown-item" href="{{ route('logout') }}"
           onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
            Logout
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
    </div>
</li>