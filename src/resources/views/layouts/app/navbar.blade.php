<?php
/**
 * Created by PhpStorm.
 * User: Callum Carmicheal
 * Date: 20/02/2018
 * Time: 09:48
 *
 * Includes:
 *      layouts.app.navbar.guest_left
 *      layouts.app.navbar.guest_right
 *      layouts.app.navbar.user_left
 *      layouts.app.navbar.user_right
 *
 * Yields:
 *      template/header
 */?>

<div class="navbar navbar-expand-lg fixed-top navbar-dark bg-primary" style="">
    <div class="container">
        <a href="../" class="navbar-brand">
            @yield('template/header', config('app.name', 'CONFIG->APP.name Fallback 3'))
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation" style="">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarResponsive">
            <form class="nav navbar-nav flex-fill justify-content-center d-md-block d-lg-none" id="frmSearch_Small">
                <div class="input-group" style="margin-top: 15px; margin-bottom: 8px;">
                    <input type="text" class="form-control" placeholder="Search Content">
                    <span class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button">GO</button>
                    </span>
                </div>
            </form>

            <ul class="navbar-nav">
                @guest
                    @include('layouts.app.navbar.guest_left')
                @else
                    @include('layouts.app.navbar.user_left')
                @endguest
            </ul>

            {{-- Search Bar has been removed because there will not be a implementation ready
                    for the deadline.--}}
            {{--
            <form class="nav navbar-nav flex-fill justify-content-center d-none d-lg-flex" id="frmSearch_Large" style="max-width:500px; margin: 0 auto;">
                <div class="input-group" style="padding-left: 50px; padding-right: 50px;">
                    <input type="text" class="form-control" placeholder="Search Content">
                    <span class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button">GO</button>
                    </span>
                </div>
            </form> --}}

            <ul class="nav navbar-nav ml-auto">
                @guest
                    @include('layouts.app.navbar.guest_right')
                @else
                    @include('layouts.app.navbar.user_right')
                @endguest
            </ul>

        </div>
    </div>
</div>