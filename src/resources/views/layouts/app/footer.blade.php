<?php
/**
 * Created by PhpStorm.
 * User: Callum Carmicheal
 * Date: 20/02/2018
 * Time: 10:06
 */

use App\Models\Extensions\WebsiteSettings;

// Get the registration state
$registrationEnabled = WebsiteSettings::settings()->registrationEnabled(); ?>

<footer class="footer">
    <div class="container">
        <p class="float-right footer-backtotop">
            <a href="#">Back to top</a>
        </p>
        <p>Cylde Timetable, Developed by Callum Carmicheal 2018&copy;</p>

        {{-- Check if we are not logged in --}}
        @if(!auth()->check())
            <p>
                Don't have a account?
                @if ($registrationEnabled)
                    <a href="{{route('register')}}">Register</a>.
                    (Please note that you will have to confirm your email and be manually activated to gain access)
                @else
                    {{-- TODO: Contact Support Button --}}
                    <a href="#">Contact Support</a>.
                @endif
            </p>
        @endif
    </div>
</footer>