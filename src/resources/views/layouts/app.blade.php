<?php
/**
 * Created by PhpStorm.
 * User: callu
 * Date: 18/02/2018
 * Time: 09:48
 *
 * Includes:
 *      layouts.app.navbar
 *      layouts.app.footer
 *
 * Yields:
 *      template/title      - Page Title.
 *      styles              - Page stylesheets.
 *      content             - The content of the page.
 *      scripts             - Any per page scripts.
 */?>

<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="lara-root" content="{{ URL::to('/') }}">

    <title>{{ config('app.name', 'CONFIG->APP.name Fallback 2') }} - @yield('template/title', 'Timetable Manager')</title>

	<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css" />

    <link href="{{ asset('assets/css/build/framework.css') }}"              rel="stylesheet">
    <link href="{{ asset('assets/css/build/app.css') }}"                    rel="stylesheet">
    <link href="{{ asset('assets/vendor/bootstrap-clockpicker.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" />
    
    @yield('styles')
</head>
<body>

    @include('layouts.app.navbar')

    <div id="app" class="container">
        @yield('content')
    </div>

    @include('layouts.app.footer')

    <!-- Scripts -->
    <script>
        window.App = {
            Laravel: {
                Root: "{{ URL::to('/') }}",
                CSRF: "{{ csrf_token() }}"
            },

            // Default Timetable Data
            Timetable: {
                Loaded: false,
                Data:   null
            },

            // Setup the objects array
            Modals: {
                ResourceManager: null,
            }
        }; // Setup window.App as a object
    </script>

    @yield('scripts/framework')
    <script src="{{ asset('assets/js/build/js/app.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <script src="{{ asset('assets/vendor/bootstrap-clockpicker.min.js') }}"></script>

    @yield('scripts/pre')
    <script src="{{ asset('assets/js/build/ts/app.js') }}"></script>
    @yield('scripts')

</body>
</html>
