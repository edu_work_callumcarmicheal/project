<?php
    /** @var $timetable App\Models\Timetable */
?>

@extends('layouts.app')

@section('content')
    <!-- Breadcrumbs -->
    <div class="header-breadcrumbs">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">{{ $timetable->name }}</a></li>
                <li class="breadcrumb-item active">Manage</li>
            </ol>
        </div>
    </div>

    <!-- Page Container / App -->
    <div class="app container" id="app-body">
        <!-- Page Header -->
        <div class="page-header" id="banner">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <h1>{{ $timetable->name }}</h1>
                    <p class="lead">{{ $timetable->description }}</p>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-6">Timetable details, Courses = NN, Subjects = NN etc.</div>
            </div>
        </div>

        <div style="height:20px;"></div>

        <modal-edit-slot></modal-edit-slot>
        <modal-resource-selection></modal-resource-selection>
        <timetable-component
                :timetable-id="{{$timetable->id}}" :readonly="false" ></timetable-component>
    </div>

@endsection

@section('styles')
    <style>
        .subject-separator {
            height: 5px;
        }

        .course-separator {
            height: 15px;
            /*box-shadow: 0 8px 20px -6px grey;*/
        }

        .after-course-separator {
            height: 15px;
        }

        .timetable-col {
            border: 1px solid #ddd;
        }

        .timetable-col.column2 {
            width: 90px;
        }

        .timetable-col.column3,
        .timetable-col.column4 {
            width: 80px;
        }
    </style>
@endsection

@section('scripts/framework')
    <script>
        (function(){
            function setTimetable(data) {
                window.App.Timetable.Loaded = true;
                window.App.Timetable.Data = data;
            }

            setTimetable({!! json_encode($timetable->toTimetableObject()); !!});
        })();
    </script>
@endsection

@section('scripts')
    <script>
        /*
        $(function() {
            let app = $("#app");
            var offset = $( ".sticky-header" ).offset();
            var sticky = $( ".timetable-row.head" );

            console.log("offset: ", offset);
            console.log("sticky: ", sticky);

            app.scroll(function() {
                let scrollTop = app.scrollTop();
                console.log(scrollTop, offset);

                if ( scrollTop > offset.top){
                    $('.sticky-header').addClass('fixed');

                    console.log("FIXED!");
                } else {
                    $('.sticky-header').removeClass('fixed');
                    console.log("NOT FIXED!!");
                }
            });
        }); //*/
        (function ($) {
            "use strict";
            var $tt_col = $('.timetable-col');
            $tt_col.on('mouseover', function () {
                var table1 = $(this).parent().parent().parent();
                var table2 = $(this).parent().parent();
                var row = $(this).parent();
                var column = $(this).data('column') + "";
                $(table2).find("." + column).addClass('hov-column');
                $(table1).find(".timetable-row.head ." + column).addClass('hov-column-head');
            });
            $tt_col.on('mouseout', function () {
                var table1 = $(this).parent().parent().parent();
                var table2 = $(this).parent().parent();
                var row = $(this).parent();
                var column = $(this).data('column') + "";
                $(table2).find("." + column).removeClass('hov-column');
                $(table1).find(".timetable-row.head ." + column).removeClass('hov-column-head');
            });
        })(jQuery);
    </script>
@endsection
