<?php /** @var $timetable \App\Models\Timetable */?>

@extends('layouts.app')

@section('content')

    <!-- Breadcrumbs -->
    <div class="header-breadcrumbs">
        <div class="container">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item"><a href="#">LANG-17-18</a></li>
                <li class="breadcrumb-item active">Manage</li>
            </ol>
        </div>
    </div>

    <div class="container">
        <!-- Page Header -->
        <div class="page-header" id="banner">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <h1>LANG-17-18</h1>
                    <p class="lead">Timetable Description</p>
                </div>
                <div class="col-lg-4 col-md-5 col-sm-6">Timetable details</div>
            </div>
        </div>

		<br><br>
        <timetable-component :timetable-id="{{$timetable->id}}" :readonly="true" ></timetable-component>
    </div>

@endsection


@section('scripts/framework')
<script>
    (function(){
        function setTimetable(data) {
            window.App.Timetable.Loaded = true;
            window.App.Timetable.Data = data;
        }

        setTimetable({!! json_encode($timetable->toTimetableObject()); !!});
    })();
</script>
@endsection

@section('scripts')
<script> </script>
@endsection