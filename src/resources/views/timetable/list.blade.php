@extends('layouts.app')

@section('content')
    <div class="container">
        @auth
            @foreach ($timetables['global'] as $table)
                @if ($loop->first)
                    <h1>My Timetables</h1>
                    <div class="width:100%;margin: 0 auto;">
                @endif
                    @each('timetable.list_item', $timetables['my'], 'table')
                @if ($loop->last)
                    </div>
                @endif
            @endforeach

            @foreach ($timetables['global'] as $table)
                @if ($loop->first)
                    <h1>Timetables Shared With Me</h1>
                    <div class="width:100%;margin: 0 auto;">
                @endif
                    @each('timetable.list_item', $timetables['shared'], 'table')
                @if ($loop->last)
                    </div>
                @endif
            @endforeach
        @endauth

        @foreach ($timetables['global'] as $key => $table)
            @if ($loop->first)
                <h1>Global Timetables</h1>
                <div class="width:100%;margin: 0 auto;">
            @endif
                @include ('timetable.list_item', $table)
            @if ($loop->last)
                </div>
            @endif
        @endforeach
    </div>
@endsection
