<?php

/** @var \App\Models\Timetable $table */
$color = "default";

if ($table->isPublic()) {
    $color = "default";
}

else if ($table->isSharedToMe()) {
	$color = "warning";
}

else if ($table->isTableMine()) {
    $color = "primary";
}

?>


<a href="{{url("/timetable/". $table->id)}}" style="margin: 0 auto; max-width: 700px;">
    <div class="bs-callout bs-callout-{{$color}}" style="padding: 15px;">
        <h4>{{$table->name}}</h4>
        <p>Created by {{$table->user->name}}</p>
    </div>
</a>