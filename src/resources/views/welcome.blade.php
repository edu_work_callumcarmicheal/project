@extends('layouts.app')
@section('template/title', 'Welcome')

@section('content')

@auth
    <section class="jumbotron text-center" style="background: #fff">
        <div class="container">
            <h1 class="jumbotron-heading">Timetable</h1>
            <p class="lead">Welcome {{auth()->user()->name}}, to get started select a option below.</p>
            <p>
                <a href="{{ route('timetables') }}" class="btn btn-primary">View Timetables</a>
            </p>
        </div>
    </section>
@else
    <section class="jumbotron text-center" style="background: #fff">
        <div class="container">
            <h1 class="jumbotron-heading">Timetable</h1>
            <p class="lead">please login to view your timetable or manage timetables or export them.</p>
            <p>
                <a href="{{ route('login') }}" class="btn btn-primary">Login</a>
                <a href="{{ route('timetables') }}" class="btn btn-info">View public timetable's</a>
            </p>
        </div>
    </section>
@endauth

@endsection

@section('styles')
<style>
    .navbar {
        margin-bottom: 0px!important;
    }
</style>
@endsection