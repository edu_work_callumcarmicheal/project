import Vue, {VueConstructor} from "vue";
import { TimetableView } from "./app/Lib/Timetable";
import ModalResourceSelection from "./app/Vue/Modal-Resource-Selection.vue";
import ModalEditSlot from "./app/Vue/Modal-Edit-Slot.vue";
import _ from "lodash/fp"

declare global {
    interface Window {
        App: ApplicationStorage;
    }

    interface ApplicationStorage {
        Laravel: { CSRF: string, Root: string };
        Timetable: { Loaded: boolean, Data: TimetableView };
        Vue: Vue;
        Objects: {}
        Modals: {
            ResourceSelection: ModalResourceSelection;
            EditSlot: ModalEditSlot;
        }
    }
}