// Import query
console.log("Importing $...");
import $ from 'jquery';
import Vue from 'vue';

// Lets setup the ajax headers
console.log("Setting ajax headers...");

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

console.log("Importing LoadTimetableTemplates...");
import "./app/Vue/Load";

// Finished loading now load our vue container
window.App.Vue = new Vue({
    el: '#app'
});