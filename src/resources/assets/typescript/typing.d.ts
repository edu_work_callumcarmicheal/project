interface Object {
    assign(target: any, ...sources: any[]): any;
}

declare enum BootstrapTableTrigger {
    Left = "left",
    Right = "right",
    Both = "both"
}

interface BootstrapTableSettings {
    contextMenu: string;
    contextMenuButton: string;
    contextMenuTrigger: BootstrapTableTrigger;
    contextMenuAutoClickRow: boolean;
    beforeContextMenuRow: Function;
}

interface JQuery {
    bootstrapTable(BootstrapTableSettings): JQuery;
    showContextMenu(): void;
    onContextMenuRow(): void;
    onContextMenuItem(): void;
}