/**
 * @author        Callum Carmicheal
 *
 * @description
 *      Provides a bunch of helper classes
 *      and wraps the responses from the server
 *      into classes that are mutable with functions.
 * @description!
 */

/**
 * Status of the timetable
 */
export enum TimetableState {
	Invalid         = -1,
	Error           = 0,  // Displayed when there is a error loading
	DoesNotExist    = 1,  // Displayed when the timetable does not exist
	Initializing    = 2,
	Loading         = 3,
	Ready           = 4
}

export enum TableVisibility {
	Private      = "private",
	PublicAuth   = "public_auth",
	PublicGuest  = "public_auth"
}

export class TimetableView {
	public id: number;
	public name: string;
	public created_by: string;
	public visibility: TableVisibility;
	public courses: CourseView[];

	/**
	 * If the timetable has any slots
	 * @type {boolean}
	 */
	private m_hasCourses: boolean = false;

	/**
	 * Checks if the timetable has any slots
	 * @returns {boolean}
	 */
	public hasCourses(): boolean { return this.m_hasCourses; }

	/**
	 * Sanitise and inject functions into table from the
	 * server.
	 *
	 * @param {TimetableView} dataResponse
	 * @returns {TimetableView}
	 * @constructor
	 */
	public static PrepareTable(dataResponse: TimetableView): TimetableView {
		let $new: TimetableView = new TimetableView();
		let $final: TimetableView = Object.assign($new, dataResponse);

		// If we dont have courses set m_hasCourses to false
		if ($final.courses === undefined ) {
			$final.courses = [];
			$final.m_hasCourses = false;
		}

		// We have courses so now
		// we want to redefine them
		else {
			// We have courses
			$final.m_hasCourses = true;

			for (let x = 0; x < $final.courses.length; x++) {
				// Redefine the course object
				// (attaching our functions to the data from the server)
				const course: CourseView = $final.courses[x];
				let $courseNew: CourseView = new CourseView();
				let $courseFinal: CourseView = Object.assign($courseNew, course);

				// Prepare the slots
				$courseFinal = this.PrepareSlots($courseFinal);

				// Set the course
				$final.courses[x] = $courseFinal;
			}
		}

		// Return the new object with the functions
		return $final;
	}

	/**
	 * Inject functions into the slots
	 *
	 * @param {CourseView} courses
	 * @returns {CourseView}
	 * @constructor
	 */
	private static PrepareSlots(courses: CourseView): CourseView {
		// Check if we have slots
		if (courses.slots !== undefined) {
			// Add the functions to slots
			let $newDays = new SlotDayList();
			courses.slots = Object.assign($newDays, courses.slots);

			// Store the list of days to iterate
			let days = ['Monday', 'Tuesday', 'Wednesday',
						'Thursday', 'Friday', 'Saturday', 'Sunday'];

			for (let x = 0; x < days.length; x++) {
				const day = days[x];

				if (courses.slots[day] !== undefined) {
					const slotList: SlotView[] = courses.slots[day];

					for (let i = 0; i < slotList.length; i++) {
						// Get the slot
						const slot = slotList[i];

						// Generate the new views
						let $newSlot = new SlotView();
						let $newRoom = new RoomView();
						let $newStaff = new StaffView();

						// Bind the functions
						courses.slots[day][i] = Object.assign( $newSlot, slot );
						courses.slots[day][i].room = Object.assign( $newRoom, slot.room );
						courses.slots[day][i].staff = Object.assign( $newStaff, slot.staff );
					}
				}
			}
		}

		// Return the courses
		return courses;
	}
}

/**
 * The course information
 */
export class CourseView {
	public id: number;
	public name: string;
	public identifier: string;
	public start_date: string;
	public end_date: string;
	public slots: SlotDayList;

	/**
	 * Contains the amount of slots
	 */
	private m_SlotSizes: DaySlotCount;

	public hasDates()       { return this.hasStartDate() || this.hasEndDate() }
	public hasStartDate()   { return this.start_date != null }
	public hasEndDate()     { return this.end_date != null }
	public containsDays()   { return this.slots.firstDayWeekIndex() != -1; }
	public containsName()   { return this.name != null }

	public countSlots( recount: boolean = false ): DaySlotCount {
		// If we want to recount or if its null
		// make sure we allocated the slotSizes
		if (recount || this.m_SlotSizes == null)
			this.m_SlotSizes = DaySlotCount.CountSlots(this.slots);

		return this.m_SlotSizes;
	}

	public tableRowSize( isReadOnly: boolean ): number {
		console.log("calcCourseRowSize: Called!");

		// Check if we have days
		if (!this.containsDays()) {
			// If we have no days we return 1
			// this single row is to "add days"
			return 1;
		}

		// Count our slots
		this.countSlots();

		// Get the totals
		let NumberOfSlots = this.m_SlotSizes.TotalSlots;

		// If we are readonly then we just add %Dividers% to NumberOfSlots
		if (isReadOnly) { return NumberOfSlots; }

		// We are read write so we will follow the algorithm below:
		//
		//      To calculate the row size we follow this algorithm
		//
		// ROW|DATA
		// ---|-------------------------
		// 1	Monday:		1
		// 2				2
		// 3				3
		// 4				4
		// 5				5
		// 7	Tuesday: 	1
		// 8				2
		// 9 	--ADD DAY--
		//
		// Example Data:
		//      Days: 		Monday, Tuesday		=	1 Divider
		//      Slots: 		5,      2			= 	7 Total Slots
		//
		//  Number of Slots + 1 (Add day) will get the amount of
		//      rows we need.
		//
		return NumberOfSlots + 1;
	}
}

/**
 * Stores the amount of slots for a
 * given SlotDayList
 */
export class DaySlotCount {
	public Monday:     number = 0;
	public Tuesday:    number = 0;
	public Wednesday:  number = 0;
	public Thursday:   number = 0;
	public Friday:     number = 0;
	public Saturday:   number = 0;
	public Sunday:     number = 0;

	public TotalDays:  number = 0;
	public TotalSlots: number = 0;

	// Only allow DaySlotCount to be created
	// within DaySlotCount (private constructor)
	private constructor() {}

	/**
	 * Count the slots of a DaySlotList
	 *
	 * @param {SlotDayList} list
	 * @returns {DaySlotCount}
	 * @constructor
	 */
	public static CountSlots(list: SlotDayList): DaySlotCount {
		// Create the new instance of DaySlotCount
		let dsc = new DaySlotCount();
		let days = list.getDayNames();

		let totalDays  = 0;
		let totalSlots = 0;

		// Loop the days
		for (let x = 0; x < days.length; x++) {
			// Get the day
			const day: string = days[x];

			// Set the length
			dsc[day] = list[day].length;

			totalDays++;
			totalSlots += dsc[day];
		}

		// Set the totals
		dsc.TotalDays = totalDays;
		dsc.TotalSlots = totalSlots;
		return dsc;
	}
}

/**
 * Our day list
 */
export class SlotDayList {
	// List of days
	public Monday:    SlotView[];
	public Tuesday:   SlotView[];
	public Wednesday: SlotView[];
	public Thursday:  SlotView[];
	public Friday:    SlotView[];
	public Saturday:  SlotView[];
	public Sunday:    SlotView[];

	/**
	 * Get the index of the first day
	 * @returns {number}
	 */
	public firstDayWeekIndex(): number {
		// Check if any of the days exist
		let d1: boolean = !( this.Monday == null );
		let d2: boolean = !( this.Tuesday == null );
		let d3: boolean = !( this.Wednesday == null );
		let d4: boolean = !( this.Thursday == null );
		let d5: boolean = !( this.Friday == null );
		let d6: boolean = !( this.Saturday == null );
		let d7: boolean = !( this.Sunday == null );

		// Find false (a day exists)
		return ([d1, d2, d3, d4, d5, d6, d7]).indexOf(true);
	}

	/**
	 * Count the amount of days we have
	 * @returns {boolean}
	 */
	public containsDays(): boolean {
		return this.firstDayWeekIndex() != -1;
	}

	/**
	 * Count the amount of days we have
	 *
	 * If we have Monday,Thurs = 2
	 * If we have Monday = 1
	 * If we have none = 0
	 *
	 * Starts at 0 for none
	 *
	 * @returns {number}
	 */
	public countDays(): number {
		// Check if any of the days exist
		let d1: boolean = !( this.Monday == null );
		let d2: boolean = !( this.Tuesday == null );
		let d3: boolean = !( this.Wednesday == null );
		let d4: boolean = !( this.Thursday == null );
		let d5: boolean = !( this.Friday == null );
		let d6: boolean = !( this.Saturday == null );
		let d7: boolean = !( this.Sunday == null );

		// Set our counter
		let count = 0;
		let array = [d1, d2, d3, d4, d5, d6, d7];

		// Count the days
		for (let i = 0; i < array.length; i++)
			// Check if the day is not null
			if (array[i])
				// Increase our counter
				count++;

		// Return our count
		return count;
	}

	/**
	 * Get the names of the days that are available
	 *
	 * @returns {string[]}
	 */
	public getDayNames(): string[] {
		// Create the days array
		let days: string[] = [];

		// Store the days
		if (this.Monday != null)    days.push("Monday");
		if (this.Tuesday != null)   days.push("Tuesday");
		if (this.Wednesday != null) days.push("Wednesday");
		if (this.Thursday != null)  days.push("Thursday");
		if (this.Friday != null)    days.push("Friday");
		if (this.Saturday != null)  days.push("Saturday");
		if (this.Sunday != null)    days.push("Sunday");

		// Return the array
		return days;
	}

	/**
	 * Get the first available day name
	 *
	 * @returns {string}
	 */
	public getFirstDayName(): string {
		// Create a list of days
		let days = ['Monday', 'Tuesday', 'Wednesday',
			'Thursday', 'Friday', 'Saturday', 'Sunday'];

		// Get the first day index
		let fdi = this.firstDayWeekIndex();

		// If FDI == -1 that means we dont have any days
		//      return "" for no days
		// ELSE we will want to return the day from the array.
		return (fdi == -1) ? "" : days[fdi];
	}
}

/**
 * Timetable: SlotView
 */
export class SlotView {
	public id: number;
	public day: number;
	public start: string;
	public end: string;
	public room: RoomView;
	public staff: StaffView;
	public code: string;
	public subject: string;
	public occurrence: number;
	public unit: number;

	constructor() {
		this.room = new RoomView();
		this.staff = new StaffView();
	}

	public static NewFrom(original: SlotView) {
		let x = new SlotView();

		x.id = original.id;
		x.day = original.day;
		x.start = original.start;
		x.end = original.end;
		x.room = original.room;
		x.staff = original.staff;
		x.code = original.code;
		x.subject = original.subject;
		x.occurrence = original.occurrence;
		x.unit = original.unit;

		return x;
	}

	public CopyTo(target: SlotView) {
		// This is called by ajax, could cause issues.
		if (this === undefined || this == null)
			return;

        let data: SlotView = JSON.parse(JSON.stringify(this));

        data.staff = Object.assign(new StaffView(), data.staff);
        data.room  = Object.assign(new RoomView(), data.room);

		target.id = data.id;
		target.day = data.day;
		target.start = data.start;
		target.end = data.end;
		target.room = data.room;
		target.staff = data.staff;
		target.code = data.code;
		target.subject = data.subject;
		target.occurrence = data.occurrence;
		target.unit = data.unit;
	}

	public getInstanceText() {
		// This is called by ajax, could cause issues.
		if (this === undefined || this == null)
			return;

		return this.code + " (" + this.subject + ")";
	}
}

/**
 * Timetable: Room View
 */
export class RoomView {
	public id: number;
	public display: string;
	public name: string;

	public getName(): string {
		// This is called by ajax, could cause issues.
		if (this === undefined || this == null)
			return;

		if (this.name === undefined)
			return this.display;

		else if (this.name == null)
			return this.name;

		else
			return this.display + " (" + this.name + ")";
	}
}

/**
 * Timetable: Staff Information
 */
export class StaffView {
	public id: number;
	public name: string;
	public colour: string;

	public getName(): string {
		// This is called by ajax, could cause issues.
		if (this === undefined || this == null)
			return;

		return this.name;
	}

	public getStyle() {
		// This is called by ajax, could cause issues.
		if (this === undefined || this == null)
			return;

		if (this.colour === undefined || this.colour == null) {
			return {};
		}

		let backgroundColor = this.colour;
		let complimentaryText = setContrast(backgroundColor);

		return {
			backgroundColor: 'rgb(' + backgroundColor + ')',
			color: complimentaryText
        }
	}
}

export function setContrast(rgbStr: string): string {
	let rgb = rgbStr.split(',');

    // http://www.w3.org/TR/AERT#color-contrast
    var o = Math.round(((parseInt(rgb[0]) * 299) +
        (parseInt(rgb[1]) * 587) +
        (parseInt(rgb[2]) * 114)) / 1000);

    var fore = (o > 125) ? 'black' : 'white';
    return fore;
}

/**
 * Copies text to clipboard
 *
 *  Text is copied to the clipboard by creating a
 *
 * @param {string} val
 * @constructor
 */
export function CopyToClipboard(val: string): void {
// Create a new input element
	let dummy = document.createElement("input");
// Add the new element to the document body
	document.body.appendChild(dummy);
// Set the display to none
	$(dummy).css('display','none');
// Set the id to dummy_id
	dummy.setAttribute("id", "dummy_id");
// Set the value
	document.getElementById("dummy_id")['value'] = val;
// Select the element
	dummy.select();
// Run the copy command
	document.execCommand("copy");
// Delete the dummy element
	document.body.removeChild(dummy);
}