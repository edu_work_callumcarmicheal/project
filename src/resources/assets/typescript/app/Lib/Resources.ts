/**
 * @author        Callum Carmicheal
 *
 * @description
 *      Handles any objects that will be returned by
 *      the resource manager
 * @description!
 */

import {resource_collections, resource_shares, resource_collectionsFields, staff, buildings, rooms} from '../Database';
import {APICallback} from "../API";

/**
 * Defines Resource Collection Response
 */
export interface ReqResCollectionCB extends APICallback<ReqResourceCollection> {
    (results: ReqResourceCollection, status: any, jqXHR: any): void;
}

/**
 * Defines Staff Response
 */
export interface ReqStaffResourcesCB extends APICallback<ReqResourceCollection> {
    (results: ReqStaffList, status: any, jqXHR: any): void;
}

/**
 * Defines building response
 */
export interface ReqBuildingResourcesCB extends APICallback<ReqResourceCollection> {
    (results: ReqBuildingList, status: any, jqXHR: any): void;
}

/**
 * Callback for SelectResource
 */
export interface SelectResourceCb {
    (status: boolean, selection: ResourceCollectionItem): void;
}

/**
 * Callback for SelectStaff
 */
export interface SelectStaffCb {
    (status: boolean, selection: Staff): void;
}

/**
 * Callback for SelectBuilding
 */
export interface SelectBuildingCb {
    (status: boolean, selection: Building): void;
}

/**
 * Callback for SelectRoom
 */
export interface SelectRoomCb {
    (status: boolean, selection: Room): void;
}

/**
 * Extended Resource Collection that includes calculated fields
 */
export interface ResourceCollection extends resource_collections {
    creator: string; // Calculated: creator, user->name
}

/**
 * Extended Building interface that includes manually appended
 *      resources.
 */
export interface Building extends buildings {
    creator: string;   // Manually appended
    campus: string;    // Manually appended
}

/**
 * Extended Room interface that includes manually appended
 *      resources.
 */
export interface Room extends rooms {
    creator: string;
    campus: string;
}

/**
 * Extended Staff that includes calculated fields
 */
export interface Staff extends staff {
    creator: string; // Calculated: creator, user->name
}

/**
 * Defines the Resource Collection with extra information
 */
export interface SharedResourceCollection {
    id: number;
    resource_id: number;
    created_by: number;
    target: number;
    type: string;
    user_by: string;
    user_tgt: string;

    resource: ResourceCollection;
}

/**
 * Request Response: Resource Collections
 */
export interface ReqResourceCollection {
    owned: ResourceCollection[];
    shared: SharedResourceCollection[];
}

/**
 * Request Response: Staff List
 */
export interface ReqStaffList {
    output: Staff[];
}

/**
 * Request Response: Building List
 */
export interface ReqBuildingList {
    output: Building[];
}

/**
 * Request Response: Room List
 */
export interface ReqRoomList {
    output: Room[];
}

/**
 * Extended ResourceCollection with calculated fields
 */
export class ResourceCollectionItem implements ResourceCollection {
    public readonly:    boolean;
    created_at:         resource_collectionsFields.created_at;
    created_by:         resource_collectionsFields.created_by;
    creator:            string;
    id:                 resource_collectionsFields.id;
    name:               resource_collectionsFields.name;
    updated_at:         resource_collectionsFields.updated_at;
    visibility:         resource_collectionsFields.visibility;

}