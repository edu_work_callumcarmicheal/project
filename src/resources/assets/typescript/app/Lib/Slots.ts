import {ResourceCollection} from "./Resources";

/**
 * @author        Callum Carmicheal
 *
 * @description
 *      Handles any objects that will be returned by
 *      the edit slot vue
 * @description!
 */

export enum EditSlotStatus {
    Cancelled = 0,
    Delete = 1,
    Saved = 2,
    Error = 3
}

export interface EditSlotCb {
    (status: EditSlotStatus): void;
}