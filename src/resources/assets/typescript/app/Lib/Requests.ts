/**
 * Creates any API Requests
 */

// Imports
import {APIEndpointType, APIRequestBase, APICallback, APIRequestType} from "../API";
import {TimetableView} from "./Timetable";

// Requests

/**
 * Get the timetable object information
 */
export class TTRequestTable extends APIRequestBase {
    private timetableId: number;

    /**
     * Set the timetable id
     *
     * @param {number} timetableId
     */
    constructor(timetableId: number) {
        // Setup the url
        let url = "req/timetable/" + timetableId + "/table";
        super(APIRequestType.GET, APIEndpointType.Relative, url);

        this.timetableId = timetableId;
    }

    /**
     * Retrieve the information with a callback
     *
     * @param {APICallback<TimetableView>} callback
     * @constructor
     */
    public Get(callback: APICallback<TimetableView>) {
        this.request(null, callback)
    }
}