/**
 * Handles connections and
 * requests to the server
 */
import $ from 'jquery';

/**
 * The type of request for the api
 */
export enum APIRequestType {
    GET,    POST,
    PUT,    PATCH,
    DELETE, OPTIONS
}

/**
 * Defines the type of the request
 */
export enum APIEndpointType {
    Relative,
    Absolute
}

/**
 * Defines an APICallback
 */
export interface APICallback<T> {
    (results: T, status: any, jqXHR: any): void
}

/**
 * The API Request
 */
export class APIRequestBase {
    private   m_BaseUrl: string;
    protected m_RequestType: APIRequestType;
    protected m_EndpointType: APIEndpointType;
    protected m_Url: string;
    protected m_DataType: string;    // Default: json
    protected m_ContentType: string; // Default: text/json; charset=utf-8

    constructor(requestType: APIRequestType, endpointType: APIEndpointType, initialUrl: string) {
        this.m_BaseUrl  = $('meta[name="lara-root"]').attr('content');
        this.m_BaseUrl += "/";

        this.m_Url          = initialUrl;
        this.m_DataType     = "json";
        this.m_RequestType  = requestType;
        this.m_EndpointType = endpointType;
    }

    /**
     * Call Request
     * @param {Object} data
     * @param {APICallback<T>} callback
     * @param {JQuery.Ajax.ErrorCallback<any>} error
     */
    public request<T>(data: Object, callback: APICallback<T>, error: JQuery.Ajax.ErrorCallback<any> = null) {
        $.ajax({
            type:        APIRequestType[this.m_RequestType],
            url:         this.getResolvedUrl(),
            data:        data,
            contentType: this.m_ContentType,
            dataType:    this.m_DataType,
            success:     callback,
            error:       error
        });
    }

// Getters and Setters
    public getUrl(): string                             { return this.m_Url; }
    public getResolvedUrl(): string                     {
        if (this.m_EndpointType == APIEndpointType.Relative)
             return this.m_BaseUrl + this.m_Url;
        else return this.m_Url;                         }
    public getRequestType(): APIRequestType             { return this.m_RequestType; }
    public getEndpointType(): APIEndpointType           { return this.m_EndpointType; }
}


/**
 * Relative Post Request
 *
 * @param {string} url
 * @param {object} data
 * @param {APICallback<T>} callback
 * @param {JQuery.Ajax.ErrorCallback<any>} error
 * @constructor
 */
export function PostRequest<T>(url: string, data: object, callback: APICallback<T>, error: JQuery.Ajax.ErrorCallback<any> = null): void {
    let req: APIRequestBase =
        new APIRequestBase(APIRequestType.POST, APIEndpointType.Relative, url);
    req.request(data, callback, error);
}