/* tslint:disable */
/**
 * This file defines all of the database tables
 *  structures for parsing information recieved
 *  from the server / api.
 */

export type enum_visibility = 'private' | 'public_rw' | 'public_ro';
export type enum_type = 'rw' | 'ro';
export type enum_access_level = 'wr' | 'ro';
export type enum_visibility_state = 'private' | 'public_auth' | 'public_guest';

export namespace addressesFields {
    export type id = number;
    export type address = string;
    export type postcode = string;
    export type created_by = number;
    export type resource_id = number;
    export type created_at = Date | null;
    export type updated_at = Date | null;

}

export interface addresses {
    id: addressesFields.id;
    address: addressesFields.address;
    postcode: addressesFields.postcode;
    created_by: addressesFields.created_by;
    resource_id: addressesFields.resource_id;
    created_at: addressesFields.created_at;
    updated_at: addressesFields.updated_at;

}

export namespace buildingsFields {
    export type id = number;
    export type campus_id = number;
    export type address_id = number;
    export type display = string;
    export type name = string;
    export type created_by = number;
    export type created_at = Date | null;
    export type updated_at = Date | null;

}

export interface buildings {
    id: buildingsFields.id;
    campus_id: buildingsFields.campus_id;
    address_id: buildingsFields.address_id;
    display: buildingsFields.display;
    name: buildingsFields.name;
    created_by: buildingsFields.created_by;
    created_at: buildingsFields.created_at;
    updated_at: buildingsFields.updated_at;

}

export namespace campusesFields {
    export type id = number;
    export type address_id = number;
    export type name = string;
    export type created_by = number;
    export type created_at = Date | null;
    export type updated_at = Date | null;

}

export interface campuses {
    id: campusesFields.id;
    address_id: campusesFields.address_id;
    name: campusesFields.name;
    created_by: campusesFields.created_by;
    created_at: campusesFields.created_at;
    updated_at: campusesFields.updated_at;

}

export namespace coursesFields {
    export type id = number;
    export type timetable_id = number;
    export type identifier = string;
    export type name = string | null;
    export type start_date = Date | null;
    export type end_date = Date | null;
    export type created_by = number;
    export type created_at = Date | null;
    export type updated_at = Date | null;

}

export interface courses {
    id: coursesFields.id;
    timetable_id: coursesFields.timetable_id;
    identifier: coursesFields.identifier;
    name: coursesFields.name;
    start_date: coursesFields.start_date;
    end_date: coursesFields.end_date;
    created_by: coursesFields.created_by;
    created_at: coursesFields.created_at;
    updated_at: coursesFields.updated_at;

}

export namespace migrationsFields {
    export type id = number;
    export type migration = string;
    export type batch = number;

}

export interface migrations {
    id: migrationsFields.id;
    migration: migrationsFields.migration;
    batch: migrationsFields.batch;

}

export namespace occurrencesFields {
    export type id = number;
    export type unit_id = number;
    export type occurrence_number = number;
    export type label = string | null;
    export type created_by = number;
    export type created_at = Date | null;
    export type updated_at = Date | null;

}

export interface occurrences {
    id: occurrencesFields.id;
    unit_id: occurrencesFields.unit_id;
    occurrence_number: occurrencesFields.occurrence_number;
    label: occurrencesFields.label;
    created_by: occurrencesFields.created_by;
    created_at: occurrencesFields.created_at;
    updated_at: occurrencesFields.updated_at;

}

export namespace password_resetsFields {
    export type email = string;
    export type token = string;
    export type created_at = Date | null;

}

export interface password_resets {
    email: password_resetsFields.email;
    token: password_resetsFields.token;
    created_at: password_resetsFields.created_at;

}

export namespace registration_invitesFields {
    export type id = number;
    export type expires_on = Date;
    export type accepted_by = number | null;
    export type accepted_at = Date | null;
    export type email = string;
    export type name = string;
    export type token = string;
    export type created_by = number | null;
    export type systemrole_id = number | null;
    export type created_at = Date | null;
    export type updated_at = Date | null;

}

export interface registration_invites {
    id: registration_invitesFields.id;
    expires_on: registration_invitesFields.expires_on;
    accepted_by: registration_invitesFields.accepted_by;
    accepted_at: registration_invitesFields.accepted_at;
    email: registration_invitesFields.email;
    name: registration_invitesFields.name;
    token: registration_invitesFields.token;
    created_by: registration_invitesFields.created_by;
    systemrole_id: registration_invitesFields.systemrole_id;
    created_at: registration_invitesFields.created_at;
    updated_at: registration_invitesFields.updated_at;

}

export namespace resource_collectionsFields {
    export type id = number;
    export type name = string;
    export type created_by = number;
    export type visibility = enum_visibility;
    export type created_at = Date | null;
    export type updated_at = Date | null;

}

export interface resource_collections {
    id: resource_collectionsFields.id;
    name: resource_collectionsFields.name;
    created_by: resource_collectionsFields.created_by;
    visibility: resource_collectionsFields.visibility;
    created_at: resource_collectionsFields.created_at;
    updated_at: resource_collectionsFields.updated_at;

}

export namespace resource_sharesFields {
    export type id = number;
    export type resource_id = number;
    export type created_by = number;
    export type target = number;
    export type type = enum_type;
    export type created_at = Date | null;
    export type updated_at = Date | null;

}

export interface resource_shares {
    id: resource_sharesFields.id;
    resource_id: resource_sharesFields.resource_id;
    created_by: resource_sharesFields.created_by;
    target: resource_sharesFields.target;
    type: resource_sharesFields.type;
    created_at: resource_sharesFields.created_at;
    updated_at: resource_sharesFields.updated_at;

}

export namespace roomsFields {
    export type id = number;
    export type building_id = number;
    export type room_number = string;
    export type name = string | null;
    export type created_by = number;
    export type created_at = Date | null;
    export type updated_at = Date | null;

}

export interface rooms {
    id: roomsFields.id;
    building_id: roomsFields.building_id;
    room_number: roomsFields.room_number;
    name: roomsFields.name;
    created_by: roomsFields.created_by;
    created_at: roomsFields.created_at;
    updated_at: roomsFields.updated_at;

}

export namespace slotsFields {
    export type id = number;
    export type course_id = number;
    export type room_id = number;
    export type staff_id = number | null;
    export type occurrence_id = number;
    export type day = number;
    export type start_time = string;
    export type end_time = string;
    export type created_by = number;
    export type created_at = Date | null;
    export type updated_at = Date | null;

}

export interface slots {
    id: slotsFields.id;
    course_id: slotsFields.course_id;
    room_id: slotsFields.room_id;
    staff_id: slotsFields.staff_id;
    occurrence_id: slotsFields.occurrence_id;
    day: slotsFields.day;
    start_time: slotsFields.start_time;
    end_time: slotsFields.end_time;
    created_by: slotsFields.created_by;
    created_at: slotsFields.created_at;
    updated_at: slotsFields.updated_at;

}

export namespace staffFields {
    export type id = number;
    export type first_name = string;
    export type last_name = string;
    export type staff_colour = string | null;
    export type created_by = number | null;
    export type resource_id = number;
    export type created_at = Date | null;
    export type updated_at = Date | null;

}

export interface staff {
    id: staffFields.id;
    first_name: staffFields.first_name;
    last_name: staffFields.last_name;
    staff_colour: staffFields.staff_colour;
    created_by: staffFields.created_by;
    resource_id: staffFields.resource_id;
    created_at: staffFields.created_at;
    updated_at: staffFields.updated_at;

}

export namespace system_rolesFields {
    export type id = number;
    export type name = string;
    export type permissions = string;
    export type is_default = boolean | null;
    export type created_at = Date | null;
    export type updated_at = Date | null;

}

export interface system_roles {
    id: system_rolesFields.id;
    name: system_rolesFields.name;
    permissions: system_rolesFields.permissions;
    is_default: system_rolesFields.is_default;
    created_at: system_rolesFields.created_at;
    updated_at: system_rolesFields.updated_at;

}

export namespace timetablesFields {
    export type id = number;
    export type name = string;
    export type description = string;
    export type created_by = number | null;
    export type visibility_state = enum_visibility_state;
    export type password = string | null;
    export type start_date = Date;
    export type end_date = Date;
    export type created_at = Date | null;
    export type updated_at = Date | null;

}

export interface timetables {
    id: timetablesFields.id;
    name: timetablesFields.name;
    description: timetablesFields.description;
    created_by: timetablesFields.created_by;
    visibility_state: timetablesFields.visibility_state;
    password: timetablesFields.password;
    start_date: timetablesFields.start_date;
    end_date: timetablesFields.end_date;
    created_at: timetablesFields.created_at;
    updated_at: timetablesFields.updated_at;

}

export namespace timetable_shared_usersFields {
    export type id = number;
    export type timetable_id = number;
    export type user_id = number;
    export type created_by = number;
    export type access_level = enum_access_level;
    export type created_at = Date | null;
    export type updated_at = Date | null;

}

export interface timetable_shared_users {
    id: timetable_shared_usersFields.id;
    timetable_id: timetable_shared_usersFields.timetable_id;
    user_id: timetable_shared_usersFields.user_id;
    created_by: timetable_shared_usersFields.created_by;
    access_level: timetable_shared_usersFields.access_level;
    created_at: timetable_shared_usersFields.created_at;
    updated_at: timetable_shared_usersFields.updated_at;

}

export namespace unitsFields {
    export type id = number;
    export type code = string;
    export type name = string;
    export type credits = number | null;
    export type created_by = number | null;
    export type resource_id = number;
    export type created_at = Date | null;
    export type updated_at = Date | null;

}

export interface units {
    id: unitsFields.id;
    code: unitsFields.code;
    name: unitsFields.name;
    credits: unitsFields.credits;
    created_by: unitsFields.created_by;
    resource_id: unitsFields.resource_id;
    created_at: unitsFields.created_at;
    updated_at: unitsFields.updated_at;

}

export namespace usersFields {
    export type id = number;
    export type name = string;
    export type email = string;
    export type password = string;
    export type enabled = boolean;
    export type systemrole_id = number | null;
    export type remember_token = string | null;
    export type created_at = Date | null;
    export type updated_at = Date | null;

}

export interface users {
    id: usersFields.id;
    name: usersFields.name;
    email: usersFields.email;
    password: usersFields.password;
    enabled: usersFields.enabled;
    systemrole_id: usersFields.systemrole_id;
    remember_token: usersFields.remember_token;
    created_at: usersFields.created_at;
    updated_at: usersFields.updated_at;

}

export namespace user_settingsFields {
    export type id = number;
    export type user_id = number;
    export type setting = string;
    export type value = string;
    export type created_at = Date | null;
    export type updated_at = Date | null;

}

export interface user_settings {
    id: user_settingsFields.id;
    user_id: user_settingsFields.user_id;
    setting: user_settingsFields.setting;
    value: user_settingsFields.value;
    created_at: user_settingsFields.created_at;
    updated_at: user_settingsFields.updated_at;

}

export namespace website_settingsFields {
    export type id = number;
    export type setting = string;
    export type value = string;
    export type created_at = Date | null;
    export type updated_at = Date | null;

}

export interface website_settings {
    id: website_settingsFields.id;
    setting: website_settingsFields.setting;
    value: website_settingsFields.value;
    created_at: website_settingsFields.created_at;
    updated_at: website_settingsFields.updated_at;

}
