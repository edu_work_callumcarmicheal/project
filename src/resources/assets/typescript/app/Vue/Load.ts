import Vue from "vue";

import TimetableComponent from "./Timetable-Component.vue";
import TimetableCourse from "./Timetable-Course.vue";
import TimetableWidthAlloc from "./Timetable-Width-Alloc.vue";
import TimetableDay from "./Timetable-Day.vue";
import TimetableAddSlot from "./Timetable-Add-Slot.vue";
import ModalResourceSelection from "./Modal-Resource-Selection.vue";
import ModalEditSlot from "./Modal-Edit-Slot.vue";

Vue.component('timetable-component', TimetableComponent);
Vue.component('timetable-course', TimetableCourse);
Vue.component('timetable-width-alloc', TimetableWidthAlloc);
Vue.component('timetable-day', TimetableDay);
Vue.component('timetable-add-slot', TimetableAddSlot);

Vue.component('modal-resource-selection', ModalResourceSelection);
Vue.component('modal-edit-slot', ModalEditSlot);