REM This will make use of the schemats script
REM   located here https://github.com/SweetIQ/schemats
REM to generate a file containing all the tables from the database

schemats generate -c mysql://timetable@localhost:3307/timetable_app -o DatabaseResults.txt