<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 16 Mar 2018 11:44:03 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Occurrence
 *
 * @property int $id
 * @property int $unit_id
 * @property int $occurrence_number
 * @property string $label
 * @property int $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \App\Models\User $user
 * @property \App\Models\Unit $unit
 * @property \Illuminate\Database\Eloquent\Collection $slots
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Occurrence whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Occurrence whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Occurrence whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Occurrence whereLabel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Occurrence whereOccurrenceNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Occurrence whereUnitId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Occurrence whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Occurrence extends Eloquent
{
	protected $casts = [
		'unit_id' => 'int',
		'occurrence_number' => 'int',
		'created_by' => 'int'
	];

	protected $fillable = [
		'unit_id',
		'occurrence_number',
		'label',
		'created_by'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}

	public function unit()
	{
		return $this->belongsTo(\App\Models\Unit::class);
	}

	public function slots()
	{
		return $this->hasMany(\App\Models\Slot::class);
	}
}
