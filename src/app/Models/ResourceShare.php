<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 16 Mar 2018 11:44:03 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ResourceShare
 *
 * @property int $id
 * @property int $resource_id
 * @property int $created_by
 * @property int $target
 * @property string $type
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \App\Models\User $user
 * @property \App\Models\ResourceCollection $resource_collection
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ResourceShare whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ResourceShare whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ResourceShare whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ResourceShare whereResourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ResourceShare whereTarget($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ResourceShare whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ResourceShare whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ResourceShare extends Eloquent
{
	protected $casts = [
		'resource_id' => 'int',
		'created_by' => 'int',
		'target' => 'int'
	];

	protected $fillable = [
		'resource_id',
		'created_by',
		'target',
		'type'
	];

	/**
	 * @return User|\Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}

	/**
	 * @return User|\Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function target()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}

	/**
	 * @return ResourceCollection|\Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function resource_collection()
	{
		return $this->belongsTo(\App\Models\ResourceCollection::class, 'resource_id');
	}

	/**
	 * Gets the resource shares for a user
	 *
	 * @param $userId
	 * @return array
	 */
	public static function getSharesFor($userId) {
		// Allocate our output variable
		$output = [];

		// Get the user by the userId
		/** @var User $user */
		$user = User::whereId($userId)->first();
		$name = $user->name;

		// Create a query
		$qry = self::where('target', $userId);

		// If count == 0 return empty
		if ($qry->count() == 0)
			return [];

		// Get the shares
		$shares = $qry->get();

		// Loop the shares
		/** @var ResourceShare $share */
		foreach ($shares as $share) {
			// Append to the output
			$output[] = [
				'id'          => $share->id,
				'resource_id' => $share->resource_id,
				'created_by'  => $share->created_by,
				'target'      => $share->target,
				'type'        => $share->type,

				'user_by'     => $share->user->name,
				'user_tgt'    => $name,

				'resource'    => $share->resource_collection
			];
		}

		// Return the output
		return $output;
	}
}
