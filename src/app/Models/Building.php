<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 16 Mar 2018 11:44:03 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Building
 *
 * @property int $id
 * @property int $campus_id
 * @property int $address_id
 * @property string $display
 * @property string $name
 * @property int $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \App\Models\User $user
 * @property \App\Models\Address $address
 * @property \App\Models\Campus $campus
 * @property \Illuminate\Database\Eloquent\Collection $rooms
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Building whereAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Building whereCampusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Building whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Building whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Building whereDisplay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Building whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Building whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Building whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Building extends Eloquent
{
	protected $casts = [
		'campus_id' => 'int',
		'address_id' => 'int',
		'created_by' => 'int'
	];

	protected $fillable = [
		'campus_id',
		'address_id',
		'display',
		'name',
		'created_by'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = ['user', 'address', 'campus'];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}

	public function address()
	{
		return $this->belongsTo(\App\Models\Address::class);
	}

	public function campus()
	{
		return $this->belongsTo(\App\Models\Campus::class);
	}

	public function rooms()
	{
		return $this->hasMany(\App\Models\Room::class);
	}
}
