<?php
/**
 * Proj: PhpStorm
 * User: CallumCarmicheal
 * Date: 12/04/2018
 * Time: 02:12
 */

namespace App\Models\Interfaces;

/**
 * Interface ITimetableObject
 *
 * @package App\Models\Interfaces
 */
interface ITimetableObject {
	/**
	 * Get the json representation for this object
	 * @return array
	 */
	public function toTimetableObject();
}
