<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 16 Mar 2018 11:44:03 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Staff
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $staff_colour
 * @property int $created_by
 * @property int $resource_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \App\Models\ResourceCollection $resource_collection
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $slots
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff whereResourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff whereStaffColour($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Staff whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Staff extends Eloquent
{
	protected $casts = [
		'created_by'  => 'int',
		'resource_id' => 'int'
	];

	protected $fillable = [
		'first_name',
		'last_name',
		'staff_colour',
		'created_by',
		'resource_id'
	];

	/**
	 * The attributes that are calculated by functions for arrays
	 *
	 * @var array
	 */
	protected $appends = array('creator');

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = ['user', 'slots', 'resource_collection'];

	public function resource_collection()
	{
		return $this->belongsTo(\App\Models\ResourceCollection::class, 'resource_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}

	public function slots()
	{
		return $this->hasMany(\App\Models\Slot::class);
	}

	public function getCreatorAttribute() {
		return $this->user->name;
	}
}
