<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 16 Mar 2018 11:44:03 +0000.
 */

namespace App\Models;

use App\Models\Interfaces\ITimetableObject;
use Carbon\Carbon;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Slot
 *
 * @property int $id
 * @property int $course_id
 * @property int $room_id
 * @property int $staff_id
 * @property int $occurrence_id
 * @property int $day
 * @property \Carbon\Carbon $start_time
 * @property \Carbon\Carbon $end_time
 * @property int $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \App\Models\User $user
 * @property \App\Models\Course $course
 * @property \App\Models\Occurrence $occurrence
 * @property \App\Models\Room $room
 * @property \App\Models\Staff $staff
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slot whereCourseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slot whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slot whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slot whereDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slot whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slot whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slot whereOccurrenceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slot whereRoomId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slot whereStaffId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slot whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Slot whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Slot extends Eloquent
	implements ITimetableObject
{
	protected $casts = [
		'course_id' => 'int',
		'room_id' => 'int',
		'staff_id' => 'int',
		'occurrence_id' => 'int',
		'day' => 'int',
		'created_by' => 'int'
	];

	protected $fillable = [
		'course_id',
		'room_id',
		'staff_id',
		'occurrence_id',
		'day',
		'start_time',
		'end_time',
		'created_by'
	];

	/**
	 * Get the user whom created this slot.
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|User
	 */
	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}

	/**
	 * Get the course this slot belongs to
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo|Course
	 */
	public function course()
	{
		return $this->belongsTo(\App\Models\Course::class);
	}
	
	/**
	 * Get the occurrence of the unit
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function occurrence()
	{
		return $this->belongsTo(\App\Models\Occurrence::class);
	}
	
	/**
	 * Get the room the slot belongs to
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function room()
	{
		return $this->belongsTo(\App\Models\Room::class);;
	}
	
	/**
	 * Get the staff member who is taking this slot
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function staff()
	{
		return $this->belongsTo(\App\Models\Staff::class);
	}
	
	/**
	 * Generate the public data for the table
	 *
	 * @return array
	 */
	public function toTimetableObject() {
		/*  id: 0,
			day: 0,
	        start: "",
	        end: "",
	        room:
				id:      -1
				display: "",
				name:    ""
	        staff:
				id:      -1
				name: "",
				color: "rrr,ggg,bbb"
	        code: "",
			subject: "",
			occurrence: -1
			unit: -1
		*/
		
		
		// Generate the room display
		$room = $this->room;
		$building = $room->building;
		$room_display = $building->display. "-". $room->room_number;
		
		// Get the Staff member,
		//      Occurrence and Unit.
		$staff = $this->staff;
		$occ   = $this->occurrence;
		$unit  = $occ->unit;

		// Format the time
		$startTime  = date("H:i", strtotime($this->start_time));
		$endTime    = date("H:i", strtotime($this->end_time));

		// Generate the course code
		$courseCode =  $unit->code. '/'.
			str_pad($occ->occurrence_number, 3,
			    '0', STR_PAD_LEFT);

		/** Stores our data for the output */
		$output = [
			'id'          => $this->id,
			'day'         => $this->day,
			'start'       => $startTime,
			'end'         => $endTime,

			// Room Information
			'room'        => [
				'id'        => $room->id,
				'display'   => $room_display,
				'name'      => $room->name
			],
			
			// Staff information
			'staff'       => [
				'id'         => $staff->id,
				'name'       => $staff->last_name. ", ". $staff->first_name,
				'colour'     => $staff->staff_colour
			],
			
			// code = UnitCode / OccNumber to 3 or more digits
			'code'       => $courseCode,
			'subject'    => $unit->name,

			'occurrence' => $occ->id,
			'unit'       => $unit->id
		];

		// Return the output
		return $output;
	}
}
