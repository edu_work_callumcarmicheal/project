<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 16 Mar 2018 11:44:03 +0000.
 */

namespace App\Models;

use App\Models\Interfaces\ITimetableObject;
use DebugBar\DebugBar;
use Doctrine\DBAL\Query\QueryBuilder;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Timetable
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $created_by
 * @property string $visibility_state
 * @property string $password
 * @property \Carbon\Carbon $start_date
 * @property \Carbon\Carbon $end_date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $courses
 * @property \Illuminate\Database\Eloquent\Collection $users
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Timetable whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Timetable whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Timetable whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Timetable whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Timetable whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Timetable whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Timetable wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Timetable whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Timetable whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Timetable whereVisibilityState($value)
 * @mixin \Eloquent
 */
class Timetable extends Eloquent
	implements ITimetableObject
{
	protected $casts = [
		'created_by' => 'int'
	];

	protected $dates = [
		'start_date',
		'end_date'
	];
	
	protected $hidden = [
		'password'
	];
	
	protected $fillable = [
		'name',
		'created_by',
		'visibility_state',
		'password',
		'start_date',
		'end_date'
	];
	
	public function user() {
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}

	public function courses() {
		return $this->hasMany(\App\Models\Course::class);
	}

	public function users() {
		return $this->belongsToMany(\App\Models\User::class, 'timetable_shared_users')
					->withPivot('id', 'created_by', 'access_level')
					->withTimestamps();
	}
	
	/**
	 * Finds all the timetables a user has created
	 *
	 * @param int $userId
	 *
	 * @return \Illuminate\Support\Collection|null
	 */
	public static function findTablesFor($userId) {
		// Attempt to find the user by id
		/** @var User $user */
		$user = User::find($userId); // Get user
		
		// Get all tables the current user created
		$tables = self::select("*")
		    ->where("created_by", '=', $user->id);

		// Retrieve the values and return them
		return $tables;
	}

	/**
	 * Get shared timetables for user
	 *
	 * @param int $userId
	 *
	 * @return $this|array
	 */
	public static function findSharedTablesFor($userId) {
		// Attempt to find the user by id
		/** @var User $user */
		$user = User::find($userId); // Get user

		// Check if the user does not exist
		if ($user == null)
			return [];

		// Get shared timetables
		$tables =
			self::select("*")
			->where("visibility_state", '=',
					'private')
			->where("created_by", '!=', $userId)
			->WhereIn('id', function($query)
					use ($user) {
				/** @var QueryBuilder $query */
				$query
				->select("timetable_id")
				->from("timetable_shared_users")
					->where('user_id', '=', $user->id);
			});

		// Retrieve the values and return them
		return $tables;
	}
	
	/**
	 * Get all global timetables (IF authed return public-auth Timetables)
	 *
	 * @param int $hideUser This will hide any user tables by passing user's id
	 *
	 * @return $this
	 */
	public static function findGlobalTimetables($hideUser = -1) {
		// Store our guest selector
		$guestSelector = [['visibility_state', '=', "public_guest"]];
		
		// Check if we are going to hide our tables
		if ($hideUser != -1)
			// Hide it by hiding created_by = $user->id
			$guestSelector[] = ['created_by', '!=', $hideUser];
		
		
		// Store our query
		$query = self::where($guestSelector);
		
		// Check if the user is logged in
		if (auth()->check()) {
			$authSelector = [['visibility_state', '=', "public_auth"]];
			
			// Check if we are going to hide our tables
			if ($hideUser != -1)
				// Hide it by hiding created_by = $user->id
				$authSelector[] = ['created_by', '!=', $hideUser];
			
			// Change the state to public-auth
			$query->orWhere($authSelector);
		}
		
		// Return the timetables
		return $query->get();
	}
	
	/**
	 * Check if the current timetable is shared
	 * to the current user / guest
	 *
	 * @return bool
	 */
	public function isSharedToMe() {
		// Check if user is logged in
		if (!auth()->check())
			// If not return null
			return true;
		
		/** @var User $user */
		$user = auth()->user(); // Get current user

		/*/
		Old code, this may come in use later

		//Check if user is logged in
		if (auth()->check()) {
			$user = auth()->user();

			$isShared = TimetableSharedUser::
				where([
					['timetable_id', '=', $this->id],
					['user_id', '=', $user->id]
				])
					->count() >= 1;

			return false;
		} //*/

		return $this->created_by != $user->id;
	}

	/**
	 * Check if the current timetable is created
	 * by the current user / guest
	 *
	 * @return bool
	 */
	public function isTableMine() {
		// Check if user is logged in
		if (!auth()->check())
			// If not return null
			return false;

		/** @var User $user */
		$user = auth()->user(); // Get current user

		return $this->created_by == $user->id;
	}

	/**
	 * Check if the current timetable is shared
	 * to the current user / guest
	 *
	 * @param bool  $isAuthenticated This means if we count public-auth as public
	 *
	 * @return bool
	 */
	public function isPublic($isAuthenticated = false) {
		if ($isAuthenticated) {
			// if we are public shared or guest shared
			return $this->visibility_state == "public_auth" ||
				$this->visibility_state == "public_guest";
		}

		// If we are guest only
		return $this->visibility_state == "public_guest";
	}

	/**
	 * Check if a user can modify this timetable
	 *
	 * @param int $userId
	 * @return bool
	 */
	public function canModify($userId = -1) {
		// If $userId = -1 then we are checking oneself's access
		if ($userId == -1) {
			// Check if user is not logged in
			if (!auth()->check())
				return false; // Cant Modify

			// Get the current user's Id
			$userId = auth()->id();
		}

		// Get the user
		$user = User::find($userId);

		// Check if the user created this timetable
		if ($user->id == $this->created_by)
			return true; // The user can modify this timetable

		// Check if the timetable is shared with this user
		$canModify = (new TimetableSharedUser)->where([
				['timetable_id', '=', $this->id],
				['user_id', '=', $user->id],
				['access_level', '=', 'wr']
			])->count() >= 1;

		return $canModify;
	}

	/**
	 * Check if a user can access this timetable
	 *
	 * @param int $userId
	 * @return bool
	 */
	public function canAccess($userId = -1) {
		// State we are checking our own access
		$checkCurrent = ($userId == -1);

		// Check if we are checking ourselfs
		// the timetable is public shared
		if ($checkCurrent && $this->isPublic(false))
			// This means practically anyone can see it
			return true;


		// Now we must check if we are checking for a guest
		// (the current user)
		if ($checkCurrent && auth()->guest())
			// We are a guest but if we reached this
			// point we don't have access
			return false;


		// Now we can get the user
		$user = null; /** @var $user User */

		// If we are getting the current
		if ($checkCurrent) {
			// We are getting the currently logged
			// in user.
			$user = auth()->user();
		}

		// We are getting the user by id
		else {
			// Get the user
			$user = User::find($userId);

			// If the user does not exist
			// return false
			if ($user == null)
				return false;
		}

		// Check if the user created this timetable
		if ($user->id == $this->created_by)
			return true; // The user can modify this timetable

		// Check if the timetable is shared with this user
		$canAccess = (new TimetableSharedUser)->where([
				['timetable_id', '=', $this->id],
				['user_id', '=', $user->id]
			])->count() >= 1;

		return $canAccess;
	}
	
	/**
	 * Get the timetable for the front end
	 * @return array
	 */
	public function toTimetableObject() {
		/* id: -1,
		   name: "",
		   created_by: "",
		   visibility: "",
		   courses:
	            id: -1,
	            name: "",
	            identifier: "",
				start_date: "",
				end_date: "",
	            slots:
		            monday:
		                id: 0,
						day: 0,
				        start: "",
				        end: "",
				        room:
							id:      -1
							display: "",
							name:    ""
				        staff:
							id:      -1
							name: "",
							color: "rrr,ggg,bbb"
				        code: "",
						subject: "",
						instance: -1
						occurrence: -1
		  */
		
		/** Stores our data for the output */
		$output = [
			'id'            => $this->id,
			'name'          => $this->name,
			'created_by'    => $this->user->name,
			'visibility'    => $this->visibility_state,
			'courses'       => []
		];
		
		/**
		 * Loop the courses using a Eloquent Cursor which can greatly
		 * reduce the processing cost of large datasets.
		 */
		foreach (Course::where('timetable_id', '=', $this->id)
                    ->cursor() as $item)
			/** @var Course $item */
			$output['courses'][] = $item->toTimetableObject();

		// Return our output
		return $output;
	}
}
