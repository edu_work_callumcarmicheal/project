<?php
/**
 * Created by PhpStorm.
 * User: callu
 * Date: 09/03/2018
 * Time: 15:51
 */

namespace App\Models\Extensions {

	use App\Models\Extensions\WebsiteSettings\Settings;


	/**
	 * Class WebsiteSettings
	 *
	 * Handles our website settings
	 *
	 * @package App\Models\Extensions
	 */
	class WebsiteSettings {
		private static $_settings;
		
		/**
		 * @return Settings
		 */
		public static function settings() {
			// Check if our settings are null
			if (self::$_settings == null)
				self::$_settings = new Settings();
			
			// Return our settings
			return self::$_settings;
		}
		
		/**
		 * TODO: Create a list of all the setting names and function to call
		 * | This will be used in the admin panel for settings.
		 */
		public static function getSettingNames() { }
	}
}
