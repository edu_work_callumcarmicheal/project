<?php
/**
 * Created by PhpStorm.
 * User: callu
 * Date: 16/03/2018
 * Time: 11:51
 */

namespace App\Models\Extensions\WebsiteSettings;

use App\Models\WebsiteSetting;

/**
 * Class Settings
 *
 * Handles and stores each individual setting
 *
 * @package App\Modesl\Extensions\WebsiteSettings
 */
class Settings {
	use SettingVariables;

	// TODO: Setup a cache system for global usage across instance
	// For now we will store this locally for recalling
	private static $_CACHE = [];

	/**
	 * Check if a setting exists in the cache
	 *
	 * @param $settingName
	 * @return bool
	 */
	private function existsCache($settingName) {
		// Check if the setting exists in the cache
		// using isset.
		return isset(self::$_CACHE[$settingName]);
	}

	/**
	 * Get the value from cache
	 *
	 * @param $settingName
	 * @return mixed
	 */
	private function getCache($settingName) {
		return self::$_CACHE[$settingName]['value'];
	}

	/**
	 * Update or store a cache variable
	 *
	 * @param $settingName
	 * @param $value
	 */
	private function setCache($settingName, $value) {
		// Set the cache value
		self::$_CACHE[$settingName] = ['value'=>$value];
	}

	/**
	 * Handles the setting request
	 *
	 * @param $settingName string
	 * @param $setToValue null|mixed
	 * @param $defaultValue mixed
	 * @param $setToDefault boolean
	 * @return mixed
	 */
	private function handleRequest($settingName, $setToValue, $defaultValue, $setToDefault) {
		// Check if we have the setting in the cache
		if ($this->existsCache($settingName)) {
			// Return the value from the cache.
			return $this->getCache($settingName);
		}

		/**
		 * Get our setting from the database
		 * @var WebsiteSetting $setting
		 */
		$setting = WebsiteSetting::firstOrNew(['setting' => $settingName]);

		// Check if our setting exists
		if (!$setting->exists) {
			// Setup the columns and save to database
			$setting->setting = $settingName;

			// Check if we are using the default value, if not check if we are setting
			// the value, if we are not then we want to revert to the default.
			$setting->value = $setToDefault ? $defaultValue : ($setToValue != null ? $setToValue : $defaultValue);
			$setting->save();

			// Set the cache
			$this->setCache($settingName, $setting->value);

			// Return the value
			return $defaultValue;
		}

		// The setting exists
		else {
			// Check if we are setting the default value
			if ($setToValue != null) {
				// Set the value and then save to the database
				$setting->value = $setToValue;
				$setting->save();
			}

			// Set the cache
			$this->setCache($settingName, $setting->value);

			// Return the value
			return $setting->value;
		}
	}
}
