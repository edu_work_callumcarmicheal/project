<?php
/**
 * Created by PhpStorm.
 * User: callu
 * Date: 16/03/2018
 * Time: 11:51
 */

namespace App\Models\Extensions\WebsiteSettings;


trait SettingVariables {
	/**
	 * Handles the setting request
	 *
	 * @param $settingName string
	 * @param $setToValue null|mixed
	 * @param $defaultValue mixed
	 * @param $setToDefault boolean
	 * @return mixed
	 */
	protected abstract function handleRequest($settingName, $setToValue, $defaultValue, $setToDefault);

	/**
	 * Check if registration is enabled
	 *
	 * @param null $value If not null set and then retrieve, if null just retrieve the value.
	 * @param bool $resetToDefault Reset the setting to the default value.
	 * @return mixed
	 */
	public function registrationEnabled($value = null, $resetToDefault = false) {
		// Setup the values and return the result
		$setting = "authentication/registration/enabled";
		$default = true;
		return self::handleRequest($setting, $value, $default, $resetToDefault);
	}
}