<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 16 Mar 2018 11:44:03 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Room
 *
 * @property int $id
 * @property int $building_id
 * @property string $room_number
 * @property string $name
 * @property int $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \App\Models\User $user
 * @property \App\Models\Building $building
 * @property \Illuminate\Database\Eloquent\Collection $slots
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Room whereBuildingId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Room whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Room whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Room whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Room whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Room whereRoomNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Room whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Room extends Eloquent
{
	protected $casts = [
		'building_id' => 'int',
		'created_by' => 'int'
	];

	protected $fillable = [
		'building_id',
		'room_number',
		'name',
		'created_by'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = ['user', 'building', 'slots'];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}

	public function building()
	{
		return $this->belongsTo(\App\Models\Building::class);
	}

	public function slots()
	{
		return $this->hasMany(\App\Models\Slot::class);
	}
}
