<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 16 Mar 2018 11:44:03 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Address
 *
 * @property int $id
 * @property string $address
 * @property string $postcode
 * @property int $created_by
 * @property int $resource_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \App\Models\ResourceCollection $resource_collection
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $buildings
 * @property \Illuminate\Database\Eloquent\Collection $campuses
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address wherePostcode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereResourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Address whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Address extends Eloquent
{
	/**
	 * Automatically casts the columns to data types
	 *
	 * @var array
	 */
	protected $casts = [
		'created_by' => 'int',
		'resource_id' => 'int'
	];

	/**
	 * States what information can be filled in
	 *
	 * @var array
	 */
	protected $fillable = [
		'address',
		'postcode',
		'created_by',
		'resource_id'
	];

	/**
	 * Relationship to ResourceCollection, BelongsTo -> resource_id
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function resource_collection()
	{
		return $this->belongsTo(\App\Models\ResourceCollection::class, 'resource_id');
	}

	/**
	 * Relationship to User, BelongsTo -> created_by
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}

	/**
	 * Relationship to Building, Has Many
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function buildings()
	{
		return $this->hasMany(\App\Models\Building::class);
	}

	/**
	 * Relationship to Campus, Has Many
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function campuses()
	{
		return $this->hasMany(\App\Models\Campus::class);
	}
}
