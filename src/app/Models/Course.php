<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 16 Mar 2018 11:44:03 +0000.
 */

namespace App\Models;

use App\Models\Interfaces\ITimetableObject;
use Carbon\Carbon;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Course
 *
 * @property int $id
 * @property int $timetable_id
 * @property string $identifier
 * @property string $name
 * @property \Carbon\Carbon $start_date
 * @property \Carbon\Carbon $end_date
 * @property int $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \App\Models\User $user
 * @property \App\Models\Timetable $timetable
 * @property \Illuminate\Database\Eloquent\Collection $slots
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Course whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Course whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Course whereEndDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Course whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Course whereIdentifier($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Course whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Course whereStartDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Course whereTimetableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Course whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Course extends Eloquent
	implements ITimetableObject
{
	protected $casts = [
		'timetable_id' => 'int',
		'created_by' => 'int'
	];

	protected $fillable = [
		'timetable_id',
		'identifier',
		'name',
		'start_date',
		'end_date',
		'created_by',
		'start_date',
		'end_date'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}

	public function timetable()
	{
		return $this->belongsTo(\App\Models\Timetable::class);
	}

	public function slots()
	{
		return $this->hasMany(\App\Models\Slot::class);
	}
	
	public function toTimetableObject() {
		/*  id: -1,
            name: "",
            identifier: "",
			start_date: "",
			end_date: "",
            slots:
	            0:  (this is monday)
	                id: 0,
					day: 0,
			        start: "",
			        end: "",
			        room:
						id:      -1
						display: "",
						name:    ""
			        staff:
						id:      -1
						name: "",
						color: "rrr,ggg,bbb"
			        code: "",
					subject: "",
					instance: -1
					occurrence: -1
		 */

		// Format the date
		$startDate = $this->start_date == null ? null :
			Carbon::createFromFormat('Y-m-d', $this->start_date)->format('d-m-y');

		// Format the date
		$endDate = $this->end_date == null ? null :
			Carbon::createFromFormat('Y-m-d', $this->end_date)->format('d-m-y');

		/** Stores our data for the output */
		$output = [
			'id'            => $this->id,
			'name'          => $this->name,
			'identifier'    => $this->identifier,
			'start_date'    => $startDate,
			'end_date'      => $endDate,
			'slots'         => []
		];
		
		/**
		 * Loop the days
		 */
		for ($day = 0; $day < 7; $day++) {
			$dayName = jddayofweek($day, 1);
			
			//continue;
			
			// Create the query
			$query = Slot::where('course_id', '=', $this->id)
			             ->where('day', '=', $day);
			
			// Check if there is any results
			$count = $query->count();
			if ($count <= 0)
				// If no results loop
				continue;
			
			/**
			 * Loop the courses using a Eloquent Cursor which can greatly
			 * reduce the processing cost of large datasets.
			 */
			foreach ($query->cursor() as $item) {
				// Make sure the day index exists
				if (!isset($output['slots'][$dayName]))
					// Allocate the day in the array
					$output['slots'][$dayName] = [];
				
				/** @var Slot $item */
				// Store the data at the end of slot->{day}
				$data = $item->toTimetableObject();
				$output['slots'][$dayName][] = $data;
			}
		}

		// Return the output
		return $output;
	}
}
