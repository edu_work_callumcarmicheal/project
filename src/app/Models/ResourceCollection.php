<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 16 Mar 2018 11:44:03 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ResourceCollection
 *
 * @property int $id
 * @property string $name
 * @property int $created_by
 * @property string $visibility
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $addresses
 * @property \Illuminate\Database\Eloquent\Collection $resource_shares
 * @property \Illuminate\Database\Eloquent\Collection $staff
 * @property \Illuminate\Database\Eloquent\Collection $units
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ResourceCollection whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ResourceCollection whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ResourceCollection whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ResourceCollection whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ResourceCollection whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ResourceCollection whereVisibility($value)
 * @mixin \Eloquent
 */
class ResourceCollection extends Eloquent
{
	protected $casts = [
		'created_by' => 'int'
	];

	protected $fillable = [
		'name',
		'created_by',
		'visibility'
	];

	/**
	 * The attributes that are calculated by functions for arrays
	 *
	 * @var array
	 */
	protected $appends = array('creator');

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = ['user'];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}

	public function addresses()
	{
		return $this->hasMany(\App\Models\Address::class, 'resource_id');
	}

	public function resource_shares()
	{
		return $this->hasMany(\App\Models\ResourceShare::class, 'resource_id');
	}

	public function staff()
	{
		return $this->hasMany(\App\Models\Staff::class, 'resource_id');
	}

	public function units()
	{
		return $this->hasMany(\App\Models\Unit::class, 'resource_id');
	}

	public function getCreatorAttribute() {
		return $this->user->name;
	}

	/**
	 * Checks if a user can see
	 *
	 * @param $userId int|User
	 * @return bool
	 */
	public function userCanSee($user)
	{
		/** @var int $userId */
		$userId = -1;

		if (is_int($user))
			 $userId = $user;
		else $userId = $user->id;

		// Check if the user owns the current collection
		if ($this->created_by == $userId)
			return true;

		// Check if any resource shares for the user
		//      and this collection.
		$conditions = [
			['resource_id', '=', $this->id],
			['target', '=', $userId] ];
		$query = ResourceShare::where($conditions);

		return $query->count() >= 1;
	}

	/**
	 * Checks if a user can modify
	 *
	 * @param $userId int|User
	 * @return bool
	 */
	public function userCanModify($user)
	{
		/** @var int $userId */
		$userId = -1;

		if (is_int($user))
			 $userId = $user;
		else $userId = $user->id;

		// Check if the user owns the current collection
		if ($this->created_by == $userId)
			return true;

		// Check if any resource shares for the user
		//      and this collection.
		$conditions = [
			['resource_id', '=', $this->id],
			['target', '=', $userId],
			['type', '=', 'rw'] ];
		$query = ResourceShare::where($conditions);

		return $query->count() >= 1;
	}
}
