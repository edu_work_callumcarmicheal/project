<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 16 Mar 2018 11:44:03 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Campus
 *
 * @property int $id
 * @property int $address_id
 * @property string $name
 * @property int $created_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \App\Models\User $user
 * @property \App\Models\Address $address
 * @property \Illuminate\Database\Eloquent\Collection $buildings
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campus whereAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campus whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campus whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Campus whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Campus extends Eloquent
{
	protected $casts = [
		'address_id' => 'int',
		'created_by' => 'int'
	];

	protected $fillable = [
		'address_id',
		'name',
		'created_by'
	];



	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}

	public function address()
	{
		return $this->belongsTo(\App\Models\Address::class);
	}

	public function buildings()
	{
		return $this->hasMany(\App\Models\Building::class);
	}
}
