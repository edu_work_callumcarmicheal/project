<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 16 Mar 2018 11:44:03 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class UserSetting
 *
 * @property int $id
 * @property int $user_id
 * @property string $setting
 * @property string $value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \App\Models\User $user
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSetting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSetting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSetting whereSetting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSetting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSetting whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserSetting whereValue($value)
 * @mixin \Eloquent
 */
class UserSetting extends Eloquent
{
	protected $casts = [
		'user_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'setting',
		'value'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}
}
