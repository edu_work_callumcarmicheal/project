<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property int $systemrole_id
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \App\Models\SystemRole $system_role
 * @property \App\Models\PasswordReset $password_reset
 * @property \Illuminate\Database\Eloquent\Collection $registration_invites
 * @property \Illuminate\Database\Eloquent\Collection $timetables
 * @property \Illuminate\Database\Eloquent\Collection $user_settings
 * @package App\Models
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereSystemroleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $enabled
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEnabled($value)
 */
class User extends Authenticatable
{
	use Notifiable;

	protected $casts = [
		'systemrole_id' => 'int'
	];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden = [
		'password',
		'remember_token'
	];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'name',
		'email',
		'password',
		'enabled',
		'systemrole_id',
		'remember_token'
	];
	
	public function system_role()
	{
		return $this->belongsTo(\App\Models\SystemRole::class, 'systemrole_id');
	}

	public function password_reset()
	{
		return $this->hasOne(\App\Models\PasswordReset::class, 'email', 'email');
	}

	public function registration_invites()
	{
		return $this->hasMany(\App\Models\RegistrationInvite::class, 'created_by');
	}

	public function timetables()
	{
		return $this->hasMany(\App\Models\Timetable::class, 'created_by');
	}

	public function user_settings()
	{
		return $this->hasMany(\App\Models\UserSetting::class);
	}
}
