<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 16 Mar 2018 11:44:03 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class RegistrationInvite
 *
 * @property int $id
 * @property \Carbon\Carbon $expires_on
 * @property int $accepted_by
 * @property \Carbon\Carbon $accepted_at
 * @property string $email
 * @property string $name
 * @property string $token
 * @property int $created_by
 * @property int $systemrole_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \App\Models\SystemRole $system_role
 * @property \App\Models\User $user
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegistrationInvite whereAcceptedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegistrationInvite whereAcceptedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegistrationInvite whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegistrationInvite whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegistrationInvite whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegistrationInvite whereExpiresOn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegistrationInvite whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegistrationInvite whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegistrationInvite whereSystemroleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegistrationInvite whereToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RegistrationInvite whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RegistrationInvite extends Eloquent
{
	protected $casts = [
		'accepted_by' => 'int',
		'created_by' => 'int',
		'systemrole_id' => 'int'
	];

	protected $dates = [
		'expires_on',
		'accepted_at'
	];

	protected $hidden = [
		'token'
	];

	protected $fillable = [
		'expires_on',
		'accepted_by',
		'accepted_at',
		'email',
		'name',
		'token',
		'created_by',
		'systemrole_id'
	];

	public function system_role()
	{
		return $this->belongsTo(\App\Models\SystemRole::class, 'systemrole_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}
}
