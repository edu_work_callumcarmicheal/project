<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 16 Mar 2018 11:44:03 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Unit
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property int $credits
 * @property int $created_by
 * @property int $resource_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \App\Models\ResourceCollection $resource_collection
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $occurrences
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unit whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unit whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unit whereCredits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unit whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unit whereResourceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Unit whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Unit extends Eloquent
{
	protected $casts = [
		'credits' => 'int',
		'created_by' => 'int',
		'resource_id' => 'int'
	];

	protected $fillable = [
		'code',
		'name',
		'credits',
		'created_by',
		'resource_id'
	];

	public function resource_collection()
	{
		return $this->belongsTo(\App\Models\ResourceCollection::class, 'resource_id');
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'created_by');
	}

	public function occurrences()
	{
		return $this->hasMany(\App\Models\Occurrence::class);
	}
}
