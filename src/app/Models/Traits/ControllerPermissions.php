<?php
/**
 * Created by PhpStorm.
 * User: callu
 * Date: 16/03/2018
 * Time: 11:27
 */

namespace App\Models\Traits;


use App\Models\User;

trait ControllerPermissions
{
	/**
	 * Check if a user has a permission
	 *
	 * @param string $permission
	 * @param User   $user The user in question, Null = current user
	 *
	 * @return bool
	 */
	public function hasPermission($permission, $user = null) {
		// Check if we have supplied a user
		if ($user == null) {
			// Check if we are logged in
			if (!auth()->check())
				// We are not return false
				return false;

			// Get our current user
			/** @var User $user */
			$user = auth()->user();
		}

		// Check if the user has a system role
		if (!$user->system_role)
			return false;

		// If we have a system role
		// call hasPermission
		return $user->system_role->hasPermission($permission);
	}

	/**
	 * Check if a user has a permissions
	 *
	 * @param array $permissions
	 * @param User  $user The user in question, Null = current user
	 *
	 * @return bool
	 */
	public function hasPermissions($permissions, $user = null) {
		// Check if we have supplied a user
		if ($user == null) {
			// Check if we are logged in
			if (!auth()->check())
				// We are not return false
				return false;

			// Get our current user
			/** @var User $user */
			$user = auth()->user();
		}

		// Check if the user has a system role
		if (!$user->system_role)
			return false;

		// If we have a system role
		// call hasPermission
		return $user->system_role->hasPermissions($permissions);
	}
}