<?php
/**
 * Proj: App
 * User: CallumCarmicheal
 * Date: 11/03/2018
 * Time: 02:08
 */

namespace App\Models\Traits;

/*
	// DOC: Include SystemRoles
	// User our permissions trait to store
	// the permissions in a different file for
	// simplicity
	use SystemPermissions;
*/

/**
 * Class SystemPermissions
 *
 * Defines the permission functions and any
 * permissions to be accessed via a function.
 *
 * @package App\Models\Traits
 */
trait SystemPermissions
{
	/**
	 * Check if the system role has a permission
	 *
	 * @param string $permission
	 * @return bool
	 */
	public abstract function hasPermission($permission);
	
	/**
	 * Check if the system role has all wanted permissions
	 *
	 * @param string[] $permissions
	 * @return bool
	 */
	public abstract function hasPermissions($permissions);
	
	// TODO: hasOnePermission(permissions: string[]): boolean
	
	/**
	 * Add a permission to the current role
	 *
	 * @param string $permission
	 * @param bool $save
	 */
	public abstract function addPermission($permission, $save = true);
	
	/**
	 * Add a permission to the current role
	 *
	 * @param string $permission
	 * @param bool $save
	 */
	public abstract function removePermission($permission, $save = true);
	
	/**
	 * @param string $permissionName
	 * @param bool $valueSet
	 * @param bool $defaultValue
	 * @param bool|null $setToDefault
	 * @return bool
	 */
	private function handleRequest(string $permissionName, $valueSet, bool $defaultValue, bool $setToDefault = false) {
		// Check if we have the permission.
		$have = $this->hasPermission($permissionName);
		
		// if not then check if we want to create it
		if (!$have && ($setToDefault || $valueSet != null)) {
			// Get our value to set
			$value = $setToDefault ? $defaultValue
						: $valueSet == null ? $defaultValue
							: $valueSet;
			
			// Add the permission
			if ($value) {
				$this->addPermission($permissionName, true);
				return true;
			}
			
			// Else, Remove the permission
			$this->removePermission($permissionName, true);
			return false;
		}
		
		// Return if we have the permission if were not setting it.
		return $have;
	}
	
	/**
	 * Check if the role can access the crud menu
	 *
	 * @param bool|null $value
	 * @param bool $default Sets the permission to the default state
	 * @return bool
	 */
	public function canAccessCRUD($value = null, $default = false) {
		return $this->handleRequest( "crud/access", $value, false, $default);
	}
}