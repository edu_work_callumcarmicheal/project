<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 16 Mar 2018 11:44:03 +0000.
 */

namespace App\Models;

use App\Models\Traits\SystemPermissions;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SystemRole
 *
 * @property int $id
 * @property string $name
 * @property string $permissions
 * @property bool $is_default
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Illuminate\Database\Eloquent\Collection $registration_invites
 * @property \Illuminate\Database\Eloquent\Collection $users
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemRole whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemRole whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemRole whereIsDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemRole whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemRole wherePermissions($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SystemRole whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SystemRole extends Eloquent
{
	// DOC: Include SystemRoles
	// User our permissions trait to store
	// the permissions in a different file for
	// simplicity
	use SystemPermissions;

	protected $casts = [
		'is_default' => 'bool'
	];

	protected $fillable = [
		'name',
		'permissions',
		'is_default'
	];

// Relationships

	public function registration_invites()
	{
		return $this->hasMany(\App\Models\RegistrationInvite::class, 'systemrole_id');
	}

	public function users()
	{
		return $this->hasMany(\App\Models\User::class, 'systemrole_id');
	}


// Permissions

	private $m_parsedPermissions = false;
	private function parsePermissions() {
		// If we have already parsed our permissions
		if ($this->m_parsedPermissions)
			return;

		// Generate our permissions
		$this->m_Permissions = explode(';', $this->permissions);
		$this->m_parsedPermissions = true;
	}

	/**
	 * Check if the system role has a permission
	 *
	 * @param string $permission
	 * @return bool
	 */
	public function hasPermission($permission) {
		// Parse our permissions
		$this->parsePermissions();

		// Check if the permissions is a astrix (Our wildcard for all perms)
		if (strcmp($this->permissions, '*') == 0)
			return true;

		// Check if we have the permission
		return in_array($permission, $this->m_Permissions);
	}

	/**
	 * Check if the system role has all wanted permissions
	 *
	 * @param string[] $permissions
	 * @return bool
	 */
	public function hasPermissions($permissions) {
		// Parse our permissions
		$this->parsePermissions();

		// Check if the permissions is a astrix (Our wildcard for all perms)
		if (strcmp($this->permissions, '*') == 0)
			return true;

		// Check if we have all the permissions
		return !array_diff($permissions, $this->m_Permissions);
	}

	/**
	 * Add a permission to the current role
	 *
	 * @param string $permission
	 * @param bool $save
	 */
	public function addPermission($permission, $save = true) {

		// Check if we are adding all permissions
		if (strcmp($permission, '*') == 0) {
			$this->permissions = "*";
			$this->m_Permissions = ['*'];
		} else {
			// Check if we don't already have the permission
			if (! $this->hasPermission($permission)) {
				// We are adding a specific permission
				// TODO: Check if permission is valid

				// Parse our permissions
				$this->parsePermissions();

				// Append the permission to our permissions
				$this->permissions .= ";$permission";

				// Append the permission to our array
				$this->m_Permissions[] = $permission;
			}

		}

		// Save changes.
		if ($save) $this->save();
	}

	/**
	 * Add a permission to the current role
	 *
	 * @param string $permission
	 * @param bool $save
	 */
	public function removePermission($permission, $save = true) {
		// Check if we are removing all permissions
		if (strcmp($permission, '*') == 0) {
			$this->permissions = "";
			$this->m_Permissions = [];
		} else {
			// We are not removing all perms
			// TODO: Check if permission is valid

			// Parse our permissions
			$this->parsePermissions();

			// Check if we have the permission
			if (!$this->hasPermission($permission))
				return;

			// Unset the permission
			unset($this->m_Permissions[$permission]);

			// Join the permissions by ";"
			$this->permissions = implode(';', $this->m_Permissions);
		}

		// Save changes.
		if ($save) $this->save();
	}
}
