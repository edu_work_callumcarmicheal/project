<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 16 Mar 2018 11:44:03 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class WebsiteSetting
 *
 * @property int $id
 * @property string $setting
 * @property string $value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteSetting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteSetting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteSetting whereSetting($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteSetting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\WebsiteSetting whereValue($value)
 * @mixin \Eloquent
 */
class WebsiteSetting extends Eloquent
{
	protected $fillable = [
		'setting',
		'value'
	];
}
