<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 16 Mar 2018 11:44:03 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class TimetableSharedUser
 *
 * @property int $id
 * @property int $timetable_id
 * @property int $user_id
 * @property int $created_by
 * @property string $access_level
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \App\Models\User $user
 * @property \App\Models\Timetable $timetable
 * @package App\Models
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TimetableSharedUser whereAccessLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TimetableSharedUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TimetableSharedUser whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TimetableSharedUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TimetableSharedUser whereTimetableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TimetableSharedUser whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TimetableSharedUser whereUserId($value)
 * @mixin \Eloquent
 */
class TimetableSharedUser extends Eloquent
{
	protected $casts = [
		'timetable_id' => 'int',
		'user_id' => 'int',
		'created_by' => 'int'
	];

	protected $fillable = [
		'timetable_id',
		'user_id',
		'created_by',
		'access_level'
	];

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class);
	}

	public function timetable()
	{
		return $this->belongsTo(\App\Models\Timetable::class);
	}
}
