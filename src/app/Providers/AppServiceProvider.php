<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
	    // Fix for compatibility with MySQL version 5.7.6 or older.
	    // SQLSTATE[42000]: Syntax error or access violation:
	    //   1071 Specified key was too long; max key length is 767 bytes
	    Schema::defaultStringLength(191);
	    
	    // Register our blade directives
	    AppProviders\BladePermission::boot();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // Register the CodeServiceProvider for Laravel Code Generation
	    if ($this->app->environment() == 'local') {
		    $this->app->register(\Reliese\Coders\CodersServiceProvider::class);
	    }
    }
}
