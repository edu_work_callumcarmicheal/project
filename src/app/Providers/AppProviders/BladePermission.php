<?php
/**
 * Proj: App
 * User: CallumCarmicheal
 * Date: 11/03/2018
 * Time: 02:53
 */

namespace App\Providers\AppProviders;

use App\Models\User;

class BladePermission
{
	/**
	 * Register our custom blade directives
	 */
	public static function boot() {
		// Check if the user has a permission
		\Blade::if('permission', function ($permission) {
			// Check if the user is logged in
			// if not return false for no perms
			if (!auth()->check())
				return false;
			
			/** @var User $user */
			$user = auth()->user(); // Get the user
			
			// Check if the user has a system_role if not
			// return false
			if ($user->system_role == null)
				return false;
			
			// We have multiple permissions to check against
			if (str_contains($permission, ',')) {
				// Remove any ", " and replace with ","
				$permission = str_replace($permission, ', ', ',');
				
				// Split the string by ","
				$perms = explode(',', $permission);
				
				// Return if the user contains all the permissions
				return $user->system_role->hasPermissions($perms);
			}
			
			// We only have one permission
			else {
				// Return if the role has that perm
				return $user->system_role->hasPermission($permission);
			}
		});
	}
}