<?php

namespace Navigation;

function activeMenuSeg($target) {
	return Request::segment(1) == $target ? 'active' : '';
}

function activeMenu($target) {
	return Request::is($target) ? 'active' : '';
}

function activeMenuName($target) {
	return Request::currentRouteNamed($target) ? 'active' : '';
}