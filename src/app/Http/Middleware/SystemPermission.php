<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;

class SystemPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $permission
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
    	// If the user is not logged in redirect to login with message
    	if (!auth()->check())
    		return redirect()
		         ->route('login')
		         ->with('message', 'You need to be logged into to do that action!');
	    
    	/** @var User $user */
    	$user = auth()->user();
    	
    	if ($user->system_role == null || !$user->system_role->hasPermission($permission)) {
		    return redirect()
			    ->route('home')
			    ->with('message', 'You do not have the required permissions for this action!');
	    }
    	
        return $next($request);
    }
}
