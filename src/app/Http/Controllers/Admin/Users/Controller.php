<?php

namespace App\Http\Controllers\Admin\Users;

use App\Models\Traits\ControllerPermissions;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, ControllerPermissions;

    public function __construct($permission)
    {
    	// ADd the middleware permission
	    $this->middleware(["perm:$permission"]);
    }
}
