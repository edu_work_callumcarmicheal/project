<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mockery\Exception;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() { }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
    	// Display home view
		return view('home');
    }

    /**
     * Show the test page
     *
     * public  = Enabled
     * private = Disabled
     *
     * @return \Illuminate\Http\Response
     */
    private function test() {
    	// Display test template
    	return view('test');
    }
}
