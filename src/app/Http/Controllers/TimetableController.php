<?php
/**
 * Proj: PhpStorm
 * User: CallumCarmicheal
 * Date: 18/03/2018
 * Time: 02:26
 */

namespace App\Http\Controllers;

use App\Models\Timetable;
use App\Models\User;
use Illuminate\Http\Request;

class TimetableController extends Controller {
	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
    public function __construct() { /*We are not instantiating objects.*/ }
	
	/**
	 * Show a list of timetables
	 */
	public function listTables() {
		// Check if the user is a guest
		//      if so, only show global timetables.
		if (auth()->guest()) {
			$global = Timetable::findGlobalTimetables();
			$timetables = [
				'global' => $global
			];
		}

		// The user is logged in
		//   so display the global, shared and
		//   owned timetables.
	    else {
		    // Get current user
		    $user = auth()->user();

		    // Query all the timetables i have
		    $myTimetables = Timetable::findTablesFor($user->id)->get();
		    $sharedTimetables = Timetable::findSharedTablesFor($user->id)->get();
		    $global = Timetable::findGlobalTimetables($user->id);

		    $timetables = [
		    	// Timetables owned by the current user
			    'my'     => $myTimetables,

			    // Timetables shared by the current user
			    'shared' => $sharedTimetables,

			    // Global timetables
			    'global' => $global
		    ];
	    }

		// Return our view to list the tables
		$data = [
			// The timetable list
			'timetables' => $timetables,

			// Template information
			'template' => [ /*Active tab to Timetable*/ "active" => "timetable" ]
		];

		// Return the view with the data
		return view("timetable.list", $data);
	}


	/**
	 * View a timetable
	 *
	 * If the timetable is not found a 404 is automatically
	 * thrown by the framework because we have binded
	 * the App/Models/Timetable object to the parameter $timetable.
	 *
	 * @param Timetable $timetable
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	public function viewTable(Timetable $timetable) {
		// Our default / fallback timetable view
		//      this is read only.
		$template = "timetable.view_ro";

		// Make sure the current user can access the timetable
		if (!$timetable->canAccess())
			// Abort Code: 404 not found
			//  return html error 404 if the timetable
			//  cant be accessed.
			abort(404);


		// Check if the user is logged in, if so then
		//  check if they can modify the timetable.
		if (auth()->check()) {
			// Check if the timetable is shared to me
			$user = auth()->user();

			// Check if the user can modify the timetable
			if ($timetable->canModify($user->id))
				// Change the template to read write.
				$template = "timetable.view_rw";
		}

		// Load the view and the
		return view($template, [
			// The timetable data
			'timetable' => $timetable,

			// Template information
			'template' => [ /*Active tab to Timetable*/ "active" => "timetable" ]
		]);
	}
	
	/**
	 * Get the json information for the timetable
	 *
	 * @return string
	 */
	public function generateTableJson(Timetable $timetable) {
		// Make sure the current user can access the timetable
		if (!$timetable->canAccess())
			// Abort Code: 404 not found
			//  return html error 404 if the timetable
			//  cant be accessed.
			abort(404);

		// Generate the timetable information for the
		//      client, this information will be
		//      used by any subroutines of the frontend.
		return json_encode($timetable->toTimetableObject());
	}
}