<?php
/**
 * Created by PhpStorm.
 * User: CallumCarmicheal
 * Date: 25/04/2018
 * Time: 22:11
 */

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Building;
use App\Models\Campus;
use App\Models\ResourceCollection;
use App\Models\ResourceShare;
use App\Models\Room;
use App\Models\Slot;
use App\Models\Staff;
use App\Models\Timetable;
use App\Models\User;
use Mockery\Exception;
use Request;
use PhpParser\Node\Expr\Array_;

class ResourceController extends Controller {

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() { /*We are not instantiating objects.*/ }

	/**
	 * Retrieves the user's resource collections
	 *  including the shared ones.
	 *
	 * Prerequisites:
	 * -    We are already logged in
	 */
	public function retrieveCollectionList() {
		// Get the userId
		$userId = auth()->id();

		// Setup the storage variable
		$output = ['owned' => [], 'shared' => []];

		// Get the owned resource collections
		// Check if the user has any resource collections
		if (($qry = ResourceCollection::whereCreatedBy($userId)->get())->count() >= 1)
			$output['owned'] = $qry->all(); // Store the collections

		// Get timetable shares
		$output['shared'] = ResourceShare::getSharesFor($userId);

		// Return the resource collections
		return $output;
	}

	/**
	 * Retrieves the staff list for a resource collection
	 *
	 * Prerequisites:
	 * -    We are already logged in
	 *
	 * @param $rc ResourceCollection
	 * @return array
	 */
	public function retrieveStaffList(ResourceCollection $rc) {
		// Return the staff which the resource collection contains
		return ['output' => $rc->staff];
	}

	/**
	 * Retrieves the building list for a resource collection
	 *
	 * Prerequisites:
	 * -    We are already logged in
	 *
	 * @param ResourceCollection $rc
	 * @return array
	 */
	public function retrieveBuildingList(ResourceCollection $rc) {
		// Create the output variable
		$output = [];

		/** @var Address $a */
		// Loop the addresses
		foreach ( $rc->addresses as $a ) {
			/** @var Campus $campus */
			// Loop the campuses
			foreach ( $a->campuses as $campus ) {
				/** @var Building $building */
				// Loop the buildings
				foreach ( $campus->buildings as $building ) {
					// Convert the building to an array
					$obj = $building->toArray();

					// Store the creator and campus name
					$obj['creator'] = $building->user->name;
					$obj['campus']  = $campus->name;

					// Append the building to the output
					$output[] = $obj;
				}
			}
		}

		// Return the output
		return ['output' => $output];
	}

	/**
	 * Retrieves the building list for a resource collection
	 *
	 * Prerequisites:
	 * -    We are already logged in
	 *
	 * @param Building $b
	 * @return array
	 */
	public function retrieveRoomList(Building $b) {
		// Create the output variable
		$output = [];

		/** @var Room $room */
		// Loop the rooms
		foreach ( $b->rooms as $room ) {
			// Convert the room to a object
			$obj = $room->toArray();

			// Store the creator
			$obj['creator'] = $room->user->name;

			// Append the room to the array
			$output[] = $obj;
		}

		// Return the output
		return ['output' => $output];
	}

	/**
	 * Update slot's information
	 *
	 * @param Slot $slot
	 * @return array
	 */
	public function updateSlot(Slot $slot) {
		// Check if the current user has readwrite
		// access to the timetable

		// Get the user id
		$userId = auth()->id();

		// Get the course
		$course = $slot->course;

		// Get the timetable
		$timetable = $course->timetable;

		// Make sure the user can modify the timetable
		if (!$timetable->canModify($userId))
			abort(403);

		// Update the slot information
		try {
			// Get the post data
			$data = request()->post();

			// Update the slot information
			$slot->room_id = $data['room']['id'];
			$slot->staff_id = $data['staff']['id'];
			$slot->day = $data['day'];
			$slot->start_time = $data['start']. ":00";
			$slot->end_time = $data['end']. ":00";
			$slot->occurrence_id = $data['occurrence'];
			$slot->save(); // Save changes to db
		} catch(Exception $ex) {
			// We have an error
			return ['success' => false, 'message' => "An error occurred while updating database!"];
		}

		// Return success message
		return ['success' => true, 'message' => "Changes saved!"];
	}
}