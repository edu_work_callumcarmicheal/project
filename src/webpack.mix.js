/**
 * This file handles the rules in compiling preprocessed scripts and styles
 * like: Typescript, Javascript, Vue.js
 * like: Scss / Sass.
 */

/**
 * Load laravel mix!
 * @type {*|Api}
 */
let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Tell laravel-mix to only display a notification on a error
mix.disableNotifications();

// Store our webpack Configuration
let _mixWebPackCfg = {
    output: {
        publicPath: "/",
        chunkFilename: 'assets/js/build/chunks/[name].[chunkhash].js'
    }
};

// Set the webpack config
mix.webpackConfig(_mixWebPackCfg);

// Tell laravel mix that we want to compile app.js and app.scss
mix
	.js('resources/assets/js/app.js', 				  'public/assets/js/build/js')
	.sass('resources/assets/sass/framework.scss',     'public/assets/css/build')
	.sass('resources/assets/sass/app.scss', 	      'public/assets/css/build')
	.typeScript('resources/assets/typescript/app.ts', 'public/assets/js/build/ts')

    // Include the source maps for debugging
	.sourceMaps()

;// Finished mix
