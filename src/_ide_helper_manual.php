<?php
/**
 * Created by PhpStorm.
 * User: callu
 * Date: 23/03/2018
 * Time: 09:33
 */

namespace Illuminate\Contracts\Auth {

	use App\Models\User;

	interface Guard
	{

		/**
		 * Get the currently authenticated user.
		 *
		 * @return User|null
		 */
		public function user();
	}
}