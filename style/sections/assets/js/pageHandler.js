var $container, $footer, $navButton;

var Framework = {};


// Enums
Framework.Enums = {};
Framework.Enums.States = {};
Framework.Enums.States.UnAuth = 0;
Framework.Enums.States.Auth = 1;

Framework.State = Framework.Enums.States.UnAuth;

Framework.setNavBarLink = function(text, link) {
	$navButton.attr('data-page', link);
	$navButton.html(text);
}


$(function () {
	Framework.pwd = function() { return window.location.href.replace(/[^\\\/]*$/, ''); }
	Framework.getSectionFilePath = function(page) { return Framework.pwd() + "assets/sections/" + page + ".html"; }
	function loadPageToContainer($container, page) { $container.load(Framework.getSectionFilePath(page)); }
	
	// Get the containers 
	$content = $(".container[data-container-id='content']");
	$footer = $(".container[data-container-id='footer']");
	$navButton = $("a.nav-link[data-button-id='navbarlink']");
	
	Framework.setNavBarLink($navButton.attr('data-link-default'))
	
	loadPageToContainer($content, $content.attr('data-container-default'));
	loadPageToContainer($footer, $footer.attr('data-container-default'));
	
	// Navigation button
	$navButton.on('click', function() {
		var $this = $(this);
		var page = $this.attr("data-page");
		
		if (!isNullOrWhitespace(page)) {
			loadPageToContainer($content, page);
		}
	});
});

// Helper function
function isNullOrWhitespace( input ) {
	if (typeof input === 'undefined' || input == null) return true;
	return input.replace(/\s/g, '').length < 1;
}
