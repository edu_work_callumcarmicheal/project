define([
    // Dependencies
    'jquery',

    // App->Controller, Loaded Controllers
    './app/controllers/navbar',

    // App->Util
    './app/util/path',
], function(
    /*dep     */ $,
    /*app-ctrl*/ navbar,
    /*app-util*/ path
) {
    // The application class
    var App = function() {
        // Utils
        this.Util = {};
        this.Util.Path = path;
        
        // Get the containers 
        this.Elements = {};
        this.Elements.$Content   = $(".container[data-container-id='content']");
        this.Elements.$Footer    = $(".container[data-container-id='footer']");
        this.Elements.$NavButton = $(".container[data-container-id='navbar']");

        this.Controllers = {};
        this.Controllers.Navbar = navbar;

        // Startup
        this.Startup = function() {

        };
    };
    
    // Create the application instance
    console.log("[application] Creating app instance");
    var instance = new App();
    
    // Wait until the page is loaded and ready
    $(function(){
        console.log("[application] Calling run on the first page");
        instance.CurrentPage.run();
    });
    
    // Return the instance of the application
    return instance;
});