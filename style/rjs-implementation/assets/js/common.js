// Dependancies

requirejs.config({
    baseUrl: './assets/js',

    paths: {
        'jquery':                   'vendor/jquery-3.2.1.min',
        'tether':                   'vendor/tether.min',
        'holder':                   'vendor/holder.min',
        'bootstrap':                'vendor/bs4/bootstrap.min',
        'application':              './application'
    },

    shim: {
        'tether':                   ['jquery'],
        'bootstrap':                ['tether', 'jquery'],
        'holder':                   ['jquery']
    }
});

// Setup tether
require(['tether'], function (Tether) {
    window.Tether = Tether;
});