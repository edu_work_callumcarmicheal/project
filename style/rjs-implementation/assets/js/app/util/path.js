define([],
function() {
    var module = function() {
        this.pwd = function() { 
            return window.location.href.replace(/[^\\\/]*$/, ''); 
        };
        
        this.getSectionFilePath = function(page) {
            return this.pwd() + "assets/sections/" + page + ".html";
        };
    };

    return new module();
});