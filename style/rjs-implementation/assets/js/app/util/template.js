define([ 
	'jquery', 
	'./path'
], function($, path) {
	// Module
	var module = function() {
		
		this.Navbar = {};
		this.Content = {};
		
		this.Content.LoadSectionIntoContainer = function($container, section) {
			if ($container instanceof jQuery) {
				return false;
			}
			
			if (!$container.attr('class').contains("container")) {
				return false;
			}
			
			if (!$container )
		};
		
		this.LoadContent = function(controller) {
			require([controller], function(ctrl) {
				ctrl.run();
			});
		};
		
	};
	
	// Return instance
	return new module();
});