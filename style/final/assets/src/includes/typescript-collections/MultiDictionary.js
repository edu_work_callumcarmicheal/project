define(["require", "exports", "./util", "./Dictionary", "./arrays"], function (require, exports, util, Dictionary_1, arrays) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var MultiDictionary = (function () {
        function MultiDictionary(toStrFunction, valuesEqualsFunction, allowDuplicateValues) {
            if (allowDuplicateValues === void 0) { allowDuplicateValues = false; }
            this.dict = new Dictionary_1.default(toStrFunction);
            this.equalsF = valuesEqualsFunction || util.defaultEquals;
            this.allowDuplicate = allowDuplicateValues;
        }
        MultiDictionary.prototype.getValue = function (key) {
            var values = this.dict.getValue(key);
            if (util.isUndefined(values)) {
                return [];
            }
            return arrays.copy(values);
        };
        MultiDictionary.prototype.setValue = function (key, value) {
            if (util.isUndefined(key) || util.isUndefined(value)) {
                return false;
            }
            var array = this.dict.getValue(key);
            if (util.isUndefined(array)) {
                this.dict.setValue(key, [value]);
                return true;
            }
            if (!this.allowDuplicate) {
                if (arrays.contains(array, value, this.equalsF)) {
                    return false;
                }
            }
            array.push(value);
            return true;
        };
        MultiDictionary.prototype.remove = function (key, value) {
            if (util.isUndefined(value)) {
                var v = this.dict.remove(key);
                return !util.isUndefined(v);
            }
            var array = this.dict.getValue(key);
            if (!util.isUndefined(array) && arrays.remove(array, value, this.equalsF)) {
                if (array.length === 0) {
                    this.dict.remove(key);
                }
                return true;
            }
            return false;
        };
        MultiDictionary.prototype.keys = function () {
            return this.dict.keys();
        };
        MultiDictionary.prototype.values = function () {
            var values = this.dict.values();
            var array = [];
            for (var _i = 0, values_1 = values; _i < values_1.length; _i++) {
                var v = values_1[_i];
                for (var _a = 0, v_1 = v; _a < v_1.length; _a++) {
                    var w = v_1[_a];
                    array.push(w);
                }
            }
            return array;
        };
        MultiDictionary.prototype.containsKey = function (key) {
            return this.dict.containsKey(key);
        };
        MultiDictionary.prototype.clear = function () {
            this.dict.clear();
        };
        MultiDictionary.prototype.size = function () {
            return this.dict.size();
        };
        MultiDictionary.prototype.isEmpty = function () {
            return this.dict.isEmpty();
        };
        return MultiDictionary;
    }());
    exports.default = MultiDictionary;
});
//# sourceMappingURL=MultiDictionary.js.map