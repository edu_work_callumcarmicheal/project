var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "./Dictionary", "./util"], function (require, exports, Dictionary_1, util) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var FactoryDictionary = (function (_super) {
        __extends(FactoryDictionary, _super);
        function FactoryDictionary(defaultFactoryFunction, toStrFunction) {
            var _this = _super.call(this, toStrFunction) || this;
            _this.defaultFactoryFunction = defaultFactoryFunction;
            return _this;
        }
        FactoryDictionary.prototype.setDefault = function (key, defaultValue) {
            var currentValue = _super.prototype.getValue.call(this, key);
            if (util.isUndefined(currentValue)) {
                this.setValue(key, defaultValue);
                return defaultValue;
            }
            return currentValue;
        };
        FactoryDictionary.prototype.getValue = function (key) {
            return this.setDefault(key, this.defaultFactoryFunction());
        };
        return FactoryDictionary;
    }(Dictionary_1.default));
    exports.default = FactoryDictionary;
});
//# sourceMappingURL=FactoryDictionary.js.map