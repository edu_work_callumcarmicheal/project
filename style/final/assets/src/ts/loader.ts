/************************************************************************
 * PROJECT:                Timetable-College
 *
 * FILE NAME:              loader.ts
 *
 * DESCRIPTION:
 *       Runs any extension functions from lib. Such as Jquery, etc.
 *
 * EXPORTS:
 *       namespace  Loader
 *       function   Loader.LoadFramework(cbLoaded: VoidFunction): void
 *
 * NOTES:
 *       <Notes>
 *
 * AUTHOR:     Callum Carmicheal      START DATE:        23 Jan 2018
 *
 * NOTABLE CHANGES:
 *
 * REF NO   VERSION  DATE        WHO  DETAIL
 * SNN/NMM  $.NN.NN  DD.MM.YYYY  II   ------
 * *******  *******  **********  **   ************************************
 *                   23.01.2018  CC   File Created
 */

export namespace Loader {
    export function LoadFramework(cbLoaded: VoidFunction): void {
        console.log("[loader.ts] Loading framework and other files...");
        require([
            // Framework
            "./lib/framework/JQueryExtensions",
            "./lib/framework/Utility",

            // Other
            "./table"
        ], function(
            // Framework
            _lib_framework_JQueryExtensions,
            _lib_framework_Utility,

            // Other
            _table
        ){
            console.log("[loader.ts] -> Loaded files!");
            cbLoaded();
        });
    }
}

export namespace TestLoader {
    export class Test {
        
    }
}
