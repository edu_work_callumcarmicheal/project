/************************************************************************
 * PROJECT:                Timetable-College
 *
 * FILE NAME:              main.ts
 *
 * DESCRIPTION:
 *       Loads any extensions from the library or framework.
 *
 * NOTES:
 *       <Notes>
 *
 * AUTHOR:     Callum Carmicheal      START DATE:        21 Jan 2018
 *
 * NOTABLE CHANGES:
 *
 * REF NO   VERSION  DATE        WHO  DETAIL
 * SNN/NMM  $.NN.NN  DD.MM.YYYY  II   ------
 * *******  *******  **********  **   ************************************
 *                   21.01.2018  CC   File Created
 */
import {Modal} from "./lib/framework/ModalNavigation";
import {Lib} from "./lib/framework/Timetable";
import ModalNavigationController = Modal.ModalNavigationController;
import {Loader} from "./loader";
import TimetableAppSettings = Lib.Framework.Timetable.AppSettings;

// Order of loading:
// 0.  Window
// 1.  Main.ts
// 2.  loader.ts
// 2.1 -> Framework
// 2.2 -> Generic Page scripts
// 3.  JQuery Page Load cb in Main.ts

window.Timetable = TimetableAppSettings.CreateSettings(window.Timetable);

// Load a new instance of TimetableAppSettings with existing config
console.info("[main.ts] Loading Framework...");
Loader.LoadFramework(function() {
    console.info("[main.ts] Loading Page...");
    $(pageLoad);
});

function loadPageController() {
    if (!window.Timetable.PageController.Enabled)
        return;

    console.info("[main.ts] => Finished loading Page Controller");
}

function loadElementControllers() {
    if (!window.Timetable.EnableElementControllers)
        return;

    $("[data-type='ElementController']").each(function(index, element) {
        let $element = $(element);
        if (!$element.hasAttribute("data-controller")) return;

        let controller = $element.attr('data-controller');
        if (controller.isEmptyOrSpaced()) return;

        console.info("[main.ts]    Found Element Controller: ", controller);

        // Attempt to load it
        require(["App/ElementControllers/" + controller], function(c){
            new c.Controller($element);
        }, function (err) {
            console.error(`[main.ts]   Failed to load controller '${controller}'!`, err);
        });
    });

    console.info("[main.ts] -> Finished loading Element Controllers");
}

//
//  Page loaded
//
function pageLoad() {
    console.log("[main.ts] Loading Page Controller...");
    loadPageController();
    console.log("[main.ts] Loading Element Controllers...");
    loadElementControllers();

    let navigation = new ModalNavigationController("#mdl-TimetableEditor");
    window["modal-timetable-editor"] = navigation;

    console.log("[main.ts] Loaded!");
}
