/************************************************************************
 * PROJECT:                Timetable-College
 *
 * FILE NAME:              Timetable.ts
 *
 * DESCRIPTION:
 *       Contains the key classes for the Application.
 *
 * EXPORTS:
 *       namespace  Lib.Framework.Timetable
 *       class      Lib.Framework.Timetable.AppSettings
 *       class      Lib.Framework.Timetable.PageController
 *       class      Lib.Framework.Timetable.ElementController
 *       class      Lib.Framework.Timetable.SettingPageController
 *
 * NOTES:
 *       <Notes>
 *
 * AUTHOR:     Callum Carmicheal      START DATE:        24 Jan 2018
 *
 * NOTABLE CHANGES:
 *
 * REF NO   VERSION  DATE        WHO  DETAIL
 * SNN/NMM  $.NN.NN  DD.MM.YYYY  II   ------
 * *******  *******  **********  **   ************************************
 *                   24.01.2018  CC   File Created
 */

import Dictionary from "typescript-collections/dist/lib/Dictionary";

export namespace Lib.Framework.Timetable {

    export class AppSettings extends Dictionary<string, any> {

        public static CreateSettings(existingSettings: object): AppSettings {
            let settings = new AppSettings(existingSettings);

            return settings;
        }

        private constructor(existingSettings: object, toStrFunction?: (key: string) => any) {
            super(toStrFunction);

            this._primitiveDefaults();

            for (let key in existingSettings) {
                let value = existingSettings[key];

                this.setValue(key, value);
            }

            this._instanceDefaults();
        }

    // Properties
        // EnableElementControllers
        get EnableElementControllers():boolean { return this.getValue("EnableElementControllers"); }
        set EnableElementControllers(v:boolean){ this.setValue("EnableElementControllers",v); }

        // Controller
        get PageController():SettingPageController { return this.getValue("PageController"); }
        set PageController(v:SettingPageController){ this.setValue("PageController",v); }

    // Functions
        // This is where you define the default values for primitive values
        private _primitiveDefaults(): void {
            this.setValue("EnableElementControllers", true);
        }

        // This is where you define the default values for classes
        private _instanceDefaults(): void {
            if (this.containsKey("PageController"))
                 this.setValue("PageController", new SettingPageController(this.getValue("PageController")));
            else this.setValue("PageController", new SettingPageController());
        }
    }

    export class PageController {
        OnReady():void{};
    }

    export class ElementController {
        protected $self: JQuery;

        constructor($s: JQuery) {
            this.$self = $s;

            let t = this;
            $(function(){t.Load.call(t);});
        }

        public getElem(): JQuery {
            return this.$self;
        }
        public getElemDom(): HTMLElement {
            return this.$self[0];
        }

        protected Load(): void { }
    }

    export class SettingPageController {
        public Enabled: boolean = false;
        public Location: string = "";
        public Instance: PageController = null;

        constructor(ExistingSettings?: object) {
            if (ExistingSettings == null)
                return;

            if (ExistingSettings.hasOwnProperty("Enabled"))
                this.Enabled = ExistingSettings["Enabled"];

            if (ExistingSettings.hasOwnProperty("Location"))
                this.Location = ExistingSettings["Location"];

            if (ExistingSettings.hasOwnProperty("Instance"))
                this.Instance = ExistingSettings["Instance"];
        }
    }
}