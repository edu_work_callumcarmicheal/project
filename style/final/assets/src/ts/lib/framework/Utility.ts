/************************************************************************
 * PROJECT:                Timetable-College
 *
 * FILE NAME:              Utility.ts
 *
 * DESCRIPTION:
 *       Utility functions that are used to make programming easier,
 *       like making frequent actions into functions.
 *
 * EXTENSIONS:
 *       string     isEmptyOrSpaced(): boolean
 *
 * NOTES:
 *       <Notes>
 *
 * AUTHOR:     Callum Carmicheal      START DATE:        21 Jan 2018
 *
 * NOTABLE CHANGES:
 *
 * REF NO   VERSION  DATE        WHO  DETAIL
 * SNN/NMM  $.NN.NN  DD.MM.YYYY  II   ------
 * *******  *******  **********  **   ************************************
 *          a.00.01  21.01.2018  CC   File Created
 *          a.00.01  24.01.2018  CC   Removed export and moved it into a
 *                                    string extension.
 */

String.prototype.isEmptyOrSpaced = function(this: string): boolean {
    return this === null || this.match(/^ *$/) !== null;
};