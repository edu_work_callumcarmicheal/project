/************************************************************************
 * PROJECT:                Timetable-College
 *
 * FILE NAME:              Events.ts
 *
 * DESCRIPTION:
 *       Enables event driven programming by creating a event model.
 *
 * EXPORTS:
 *       namespace  Lib.Framework
 *       interface  Lib.Framework.IEvent<T>
 *       class      Lib.Framework.Event<T>
 *
 * NOTES:
 *       <Notes>
 *
 * AUTHOR:     Callum Carmicheal      START DATE:        21 Jan 2018
 *
 * NOTABLE CHANGES:
 *
 * REF NO   VERSION  DATE        WHO  DETAIL
 * SNN/NMM  $.NN.NN  DD.MM.YYYY  II   ------
 * *******  *******  **********  **   ************************************
 *          a.00.01  21.01.2018  CC   File Created
 */

export namespace Lib.Framework {

    export interface IEvent<T> {
        on(handler: { (data?: T): void }) : void;
        off(handler: { (data?: T): void }) : void;
    }

    export class Event<T> implements IEvent<T> {
        private handlers: { (data?: T): void; }[] = [];

        public on(handler: { (data?: T): void }) : void {
            this.handlers.push(handler);
        }

        public off(handler: { (data?: T): void }) : void {
            this.handlers = this.handlers.filter(h => h !== handler);
        }

        public trigger(data?: T) {
            this.handlers.slice(0).forEach(h => h(data));
        }

        public expose() : IEvent<T> {
            return this;
        }
    }

}
