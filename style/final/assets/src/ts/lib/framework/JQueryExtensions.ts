/************************************************************************
 * PROJECT:                Timetable-College
 *
 * FILE NAME:              JQueryExtensions.ts
 *
 * DESCRIPTION:
 *       Defines any extensions for the jquery library, such as
 *       "hasAttribute".
 *
 * EXTENSIONS:
 *       jQuery     hasAttribute(AttributeName:string): boolean
 *
 * NOTES:
 *       <Notes>
 *
 * AUTHOR:     Callum Carmicheal      START DATE:        22 Jan 2018
 *
 * NOTABLE CHANGES:
 *
 * REF NO   VERSION  DATE        WHO  DETAIL
 * SNN/NMM  $.NN.NN  DD.MM.YYYY  II   ------
 * *******  *******  **********  **   ************************************
 *                   22.01.2018  CC   File Created
 *                   22.01.2018  CC   Added hasAttribute
 */


jQuery.fn.extend({
    hasAttribute: function(AttributeName: string): boolean {
        return $(this)[0].hasAttribute(AttributeName);
    }
});