/************************************************************************
 * PROJECT:                Timetable-College
 *
 * FILE NAME:              ModalNavigation.ts
 *
 * DESCRIPTION:
 *       Allows modal's to be a page in their own right, Allows for
 *       paged modal's and gives a controller to handle the modals.
 *
 * EXPORTS:
 *       namespace  Lib.Framework
 *       class      Lib.Framework.ModalNavigationController
 *       class      Lib.Framework.ModalPage
 *       class      Lib.Framework.ModalPageController
 *
 * NOTES:
 *       <Notes>
 *
 * AUTHOR:     Callum Carmicheal      START DATE:        21 Jan 2018
 *
 * NOTABLE CHANGES:
 *
 * REF NO   VERSION  DATE        WHO  DETAIL
 * SNN/NMM  $.NN.NN  DD.MM.YYYY  II   ------
 * *******  *******  **********  **   ************************************
 *          a.00.01  21.01.2018  CC   File Created
 *          a.00.01  22.01.2018  CC   Added ModalPage
 *          a.00.01  23.01.2018  CC   Moved files into a namespace (Modal)
 *          a.00.01  23.01.2018  CC   Added ModalPageController
 */


// Imports
import * as Collections from 'typescript-collections';
import Dictionary from "typescript-collections/dist/lib/Dictionary";

export namespace Modal {

// Class: ModalNavigationController
    export class ModalNavigationController {
    // Static Id's
        // Containers
        private readonly c_mdlHeader: string = "mdl-header";
        private readonly c_mdlBody: string = "mdl-body";
        private readonly c_mdlFooter: string = "mdl-footer";
        // Display
        private readonly d_mdlHeaderTitle: string = "mdl-header-title";
        // Types
        private readonly t_Page: string = "page";
        private readonly t_Container: string = "container";
        private readonly t_DisplayText: string = "display";
        private readonly t_AnchorLink: string = "mdl-link";
        // Static selectors
        private readonly s_Pages = `> div[data-type='${this.t_Page}']`;
        private readonly s_ActivePage = `> div[data-type='${this.t_Page}'][data-active="true"]`;
        private readonly s_LinkNav = `a[href='#'][data-type='${this.t_AnchorLink}']`;
    // Variables
        private activePage: string;
        private $modal: JQuery;
        private dPages: Collections.Dictionary<string, ModalPage>;
        private dSharedVars: Collections.Dictionary<string, any>;

    // Functions
        constructor(selector: string) {
            this.$modal = $(selector);
            this.dPages = new Dictionary<string, ModalPage>();

            this._populatePages();
            this._handleNewPages();
            this._bindings();
        }

        private _bindings(): void {
            let mnc = this;

            // Link nav binding
            this.$modal.find(this.s_LinkNav).click(function () {
                let $this = $(this);
                let target = $this.attr('data-page');

                let test: String = "Hello";

                if (target.isEmptyOrSpaced())
                    return;

                mnc.changePage(target);
            });

            // TODO: Modal Open and Close!
            this.$modal.on('show.bs.modal', function(event) {
                // Run the Load event
                mnc.getActivePage().Load();
            });

            this.$modal.on('hide.bs.modal', function(event) {
               // Run the Close event
                mnc.getActivePage().Close();
            });
        }

        private _populatePages(): void {
            let $container = this.getContainerById$(this.c_mdlBody);
            let $pages = $container.find(this.s_Pages);

            let activePage = "";
            let setActivePage = false;

            // Loop all the existing pages
            for (let i = 0; i < $pages.length; i++) {
                let $e = $($pages[i]);
                let mp = new ModalPage($e, this);

                if (this.dPages.containsKey(mp.getName()))
                    console.warn(`[ModalNavigation (${this.getId()})] Possibly duplicate page (same name). ${mp.getName()}`);

                this.dPages.setValue(mp.getName(), mp);

                if (mp.isActive()) {
                    if (setActivePage) {
                        console.warn(`[ModalNavigation (${this.getId()})] There is multiple active pages, Current=${activePage}, Collision=${mp.getName()}`);
                    } else {
                        setActivePage = true;
                        activePage = mp.getName();
                    }
                }
            }

            if (setActivePage) {
                this.activePage = activePage;
            } else {
                console.warn("[ModalNavigation (${this.getId()})] No default page set, before dialog is shown a page is required to be set.");
            }
        }

        private _handleNewPages(): void {
            // Pages container
            let $container = this.getContainerById$(this.c_mdlBody);

            // On new element
            $container.arrive(this.s_Pages, function (newElem) {
                let $newElem = $(newElem);
                let mp = new ModalPage($newElem, this);

                if (this.dPages.containsKey(mp.getName()))
                    console.warn(`[ModalNavigation (${this.getId()})] Possibly duplicate page (same name). ${mp.getName()}`);

                this.dPages.setValue(mp.getName(), mp);
            });
        }

        public changePage(newPage: string | ModalPage): boolean {
            let strName = newPage instanceof ModalPage ? newPage.getName() : newPage;

            if (this.dPages.containsKey(strName)) {
                this.activePage = strName;

                // Hide the current page
                let $cActivePage = this.getContainerById$(this.c_mdlBody).find(this.s_ActivePage);
                if ($cActivePage != null) {
                    let cActivePageName = $cActivePage.attr("data-page");
                    $cActivePage.addClass('hidden').removeAttr('data-active');

                    // Check to see if we still have this page as a ModalPage
                    if (this.dPages.containsKey(cActivePageName)) {
                        let currentPage = this.dPages.getValue(cActivePageName);
                        currentPage.Leave();
                    }
                }

                this.getPageByName(this.activePage).getElem().removeClass("hidden").attr('data-active', "true");

                this.dPages.getValue(this.activePage).Shown();

                return true;
            }

            return false;
        }

        public getContainerById$(id): JQuery {
            return this.$modal.find(`div[data-id='${id}'][data-type='container']`);
        }

        public getContainerById(id): HTMLElement {
            return this.getContainerById$(id)[0];
        }

        public getElem(): JQuery {
            return this.$modal;
        }

        public getElemDom(): HTMLElement {
            return this.$modal[0];
        }

        public getPageByName(name: string): ModalPage {
            if (this.dPages.containsKey(name))
                return this.dPages.getValue(name);
            return null;
        }

        public getActivePage(): ModalPage {
            if (this.dPages.containsKey(this.activePage))
                return this.dPages.getValue(this.activePage);

            console.error(`[ModalNavigation (${this.getId()})] No activePage: ${this.activePage}`);

            return null;
        }

        public getActivePageName(): string {
            return this.activePage;
        }

        public getId(): string {
            return this.$modal.attr("id");
        }

        public rebindAll(): void {
            return this._bindings();
        }

        public show(): void { this.$modal.modal('show'); }
        public hide(): void { this.$modal.modal('hide'); }

        public saveButton_SetVisibility(state: boolean): void {
            if (state) {
                this.getContainerById$(this.c_mdlFooter).find("button[data-id='save']").addClass('hidden');
            } else {
                this.getContainerById$(this.c_mdlFooter).find("button[data-id='save']").removeClass('hidden');
            }
        }

        public getShared(): Collections.Dictionary<string, any> {
            return this.dSharedVars;
        }
    }


// Class: ModalPage
    export class ModalPage {
        private parent: ModalNavigationController;
        private $self: JQuery;
        private controller: ModalPageController = null;

        constructor(self: JQuery, parent: ModalNavigationController) {
            this.parent = parent;
            this.$self = self;

            // If we have a controller set it up
            this._setupController();
        }

        private _setupController(): void {
            // Check if we have a controller
            if (!this.$self.hasAttribute("data-controller"))
                return;

            let strController = this.$self.attr("data-controller");
            let page = this;

            if (strController.isEmptyOrSpaced())
                return;

            // Attempt to load it
            require(["App/ModalControllers/" + strController], function(c){
                page.controller = new c.Controller(page, page.getElem(), page.parent);
                //console.log("Set controller", page, page.controller);
            }, function (err) {
                console.error(`[ModalPage(${page.getName()})] Failed to load controller!`, err);
            });
        }

        public getElem(): JQuery {
            return this.$self;
        }

        public getElemDom(): HTMLElement {
            return this.getElem()[0];
        }

        public findById(id: string): HTMLElement {
            return this.findById$(id)[0];
        }

        public findById$(id: string): JQuery {
            return this.$self.find(`[data-id='${id}']`);
        }

        public getName(): string {
            return this.$self.attr("data-page");
        }

        public isActive(): boolean {
            return this.$self.attr("data-active") == "true";
        }

        public Save(): boolean {
            // If we have a controller pass it on
            if (this.controller == null)
                return false;

            return this.controller.Save();
        }
        
        public Shown(): void {
            // If we have a controller pass it on
            if (this.controller == null)
                return;

            this.controller.Shown();
        }

        public Load(): void {
            // If we have a controller pass it on
            if (this.controller == null)
                return;

            this.controller.Load();
        }

        public Leave(): void {
            // If we have a controller pass it on
            if (this.controller == null)
                return;

            this.controller.Leave();
        }

        public Close(): void {
            // If we have a controller pass it on
            if (this.controller == null)
                return;

            this.controller.Close();
        }

        public hasController(): boolean {
            return this.controller != null;
        }

        public getController(): ModalPageController {
            return this.controller;
        }
    }

// Class:
    export abstract class ModalPageController {
        protected Page: ModalPage;
        protected $self: JQuery;
        protected nav: ModalNavigationController;

        constructor(page: ModalPage, $self: JQuery, navController: ModalNavigationController) {
            this.Page = page;
            this.$self = $self;
            this.nav = navController;
        }

        // TODO: Stop double calls
        public Save(): boolean {return false;};
        public Load(): void {};
        public Close(): void {};
        public Shown(): void {};
        public Leave(): void {};

        protected print(str: string): void {
            console.info( `[${this.Page.getName()}] ${str}` );
        }
    }
}
