/********************************************************************************
 * PROJECT:                Timetable-College
 *
 * FILE NAME:              table.ts
 *
 * DESCRIPTION:
 *       Handles the highlighting of selected cells and hovered cells
 *
 * EXPORTS:
 *       <Exported constructs>
 *
 * NOTES:
 *       <Notes>
 *
 * AUTHOR:     Callum Carmicheal      START DATE:        21 Jan 2018
 *
 * NOTABLE CHANGES:
 *
 * REF NO   VERSION  DATE        WHO  DETAIL
 * SNN/NMM  $.NN.NN  DD.MM.YYYY  II   ------
 * *******  *******  **********  **   *******************************************
 *                   21.01.2018  CC   File Created.
 *                   25.01.2018  CC   Moved click handling code to App/
 *                                    ElementControllers/TimetableController.ts
 */

(function ($) {
    "use strict";

    var $tt_col = $('.timetable-col');

    $tt_col.on('mouseover',function(){
        var table1 = $(this).parent().parent().parent();
        var table2 = $(this).parent().parent();
        var row    = $(this).parent();
        var column = $(this).data('column') + "";

        $(table2).find("."+column).addClass('hov-column');
        $(table1).find(".timetable-row.head ."+column).addClass('hov-column-head');
    });

    $tt_col.on('mouseout',function(){
        var table1 = $(this).parent().parent().parent();
        var table2 = $(this).parent().parent();
        var row    = $(this).parent();
        var column = $(this).data('column') + "";

        $(table2).find("."+column).removeClass('hov-column');
        $(table1).find(".timetable-row.head ."+column).removeClass('hov-column-head');
    });
})(jQuery);
