/************************************************************************
 * PROJECT:                Timetable-College
 *
 * FILE NAME:              typings_imports.d.ts
 *
 * DESCRIPTION:
 *       Acts as a header file for typescript, adding imports into a
 *       *.d.ts file seems to cause many issues, to subvert this any
 *       definitions that require a import will eb placed into this file.
 *
 * NOTES:
 *       <Notes>
 *
 * AUTHOR:     Callum Carmicheal      START DATE:        24 Jan 2018
 *
 * NOTABLE CHANGES:
 *
 * REF NO   VERSION  DATE        WHO  DETAIL
 * SNN/NMM  $.NN.NN  DD.MM.YYYY  II   ------
 * *******  *******  **********  **   ************************************
 *                   24.01.2018  CC   File Created
 */


import {Lib} from "./lib/framework/Timetable";


// ***** Custom Window Extensions ***** //
declare global {
    interface Window {
        Timetable: Lib.Framework.Timetable.AppSettings;
    }
} //*/
