/************************************************************************
 * PROJECT:                Timetable-College
 *
 * FILE NAME:              TimetableController.ts
 *
 * DESCRIPTION:
 *       <Description of the file>
 *
 * EXPORTS:
 *       <Exported constructs>
 *
 * NOTES:
 *       <Notes>
 *
 * AUTHOR:     Callum Carmicheal      START DATE:        24 Jan 2018
 *
 * NOTABLE CHANGES:
 *
 * REF NO   VERSION  DATE        WHO  DETAIL
 * SNN/NMM  $.NN.NN  DD.MM.YYYY  II   ------
 * *******  *******  **********  **   ************************************
 *                   24.01.2018  CC   File Created
 */
import {Lib} from "../../lib/framework/Timetable";
import {Modal} from "../../lib/framework/ModalNavigation";
import {Controller as PageThreeController} from "../ModalControllers/SubTest/PageThreeController";
import {Controller as PageOneController} from "../ModalControllers/PageOneController";
import ElementController = Lib.Framework.Timetable.ElementController;
import ModalNavigationController = Modal.ModalNavigationController;

enum ColumnType {
    COURSE      = 1,   DAY       = 2,
    TIME_START  = 3,   TIME_END  = 4,
    ROOM        = 5,   STAFF     = 6,
    CODE        = 7,   SUBJECT   = 8
}

export class Controller extends ElementController {
// Variables
    private $cells: JQuery = null;
    private mnc: ModalNavigationController = null;

// Constructor and Controller Events
    constructor($s: JQuery) {
        super($s);

        this.mnc = window["modal-timetable-editor"];
    }

    Load(): void {
        let t = this;
        console.log("こんにちは、私の", this.$self);

        this.$cells = this.$self.find('.timetable-col');
        this.$cells.dblclick( function(e) { t.cell_DoubleClick.call(t, this, e); } );
    }

// Events
    private cell_DoubleClick(element: HTMLElement, event: Event) {
        let colType = this.getColumnFor(element);

        if (colType == ColumnType.COURSE) {
            if (this.mnc.changePage("page1")) {
                let page = this.mnc.getPageByName("page1");
                let controller = (page.getController() as PageOneController);

                console.log("mnc.show()");
                this.mnc.show();

                console.log("_tCurrentTime");
                controller._tCurrentTime(false);
                controller._tCurrentTime(false);
                controller._tCurrentTime(false);
                controller._tCurrentTime(false);
            }
        }

        else if (this.mnc.changePage("page3")) {
            let page = this.mnc.getPageByName("page3");
            let controller = (page.getController() as PageThreeController);

            controller.btnPageOneContent = element.innerText;

            this.mnc.show();
        }
    }

// Functions
    private getColumnNumberFor(element: HTMLElement): number {
        return Number(element.getAttribute("data-column")[6]);
    }

    private getColumnFor(element: HTMLElement): ColumnType {
        return this.getColumnNumberFor(element);
    }
}
