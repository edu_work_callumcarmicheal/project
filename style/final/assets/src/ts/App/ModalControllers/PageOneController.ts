/************************************************************************
 * PROJECT:                Timetable-College
 *
 * FILE NAME:              PageOneController
 *
 * DESCRIPTION:
 *       <Description of the file>
 *
 *
 * EXPORTS:
 *       Controller
 *
 *
 * NOTES:
 *       <Notes>
 *
 *
 * AUTHOR:     Callum Carmicheal      START DATE:        22 Jan 2018
 *
 *
 * NOTABLE CHANGES:
 *
 * REF NO   VERSION  DATE        WHO  DETAIL
 * SNN/NNM  S.NN.NN  22.012.018  CC   File Created
 */

// Imports
import MP = Modal.ModalPage;
import MNC = Modal.ModalNavigationController;
import {Modal} from "../../lib/framework/ModalNavigation";
import ModalPageController = Modal.ModalPageController;

export class Controller extends ModalPageController {
    private $pCurrentTime: JQuery;


    //
    constructor(p: MP, $s: JQuery, nc: MNC) {
        super(p, $s, nc);

        this.$pCurrentTime = $s.find("h5");
    }

// Events
    public Load(): void {
        this.print("Load");
        this._tCurrentTime(true); // Start timer
    }

    public Shown(): void {
        this.print("Shown");
        this._tCurrentTime(true); // Start timer
    }

    public Leave(): void {
        this.print("Leave");
        this._tCurrentTime(false); // Stop timer
    }

    public Close(): void {
        this.print("Close");
        this._tCurrentTime(false); // Stop timer
    }

// Functions
    private cTimer: number = -1;
    _tCurrentTime(state: boolean = true, msDelay: number = 1000): void {
        if (state == false) {
            console.debug("CLEARINTERVAL!", this.cTimer);
            clearInterval(this.cTimer);
            return;
        }


        function updateTime(): void {
            let time = new Date().toLocaleString();

            this.$pCurrentTime.html(time);

            console.log(time);
        }


        let t = this;
        updateTime.call(t);
        //this.cTimer = setInterval(function(){ updateTime.call(t); }, msDelay);
        console.debug("SETINTERVAL", this.cTimer);
    }
}