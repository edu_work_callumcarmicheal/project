/************************************************************************
 * PROJECT:                Timetable-College
 *
 * FILE NAME:              PageThreeController
 *
 * DESCRIPTION:
 *       <Description of the file>
 *
 * EXPORTS:
 *       class      Controller
 *
 * NOTES:
 *       <Notes>
 *
 * AUTHOR:     Callum Carmicheal      START DATE:        24 Jan 2018
 *
 * NOTABLE CHANGES:
 *
 * REF NO   VERSION  DATE        WHO  DETAIL
 * SNN/NMM  $.NN.NN  DD.MM.YYYY  II   ------
 * *******  *******  **********  **   ************************************
 *                   24.01.2018  CC   File Created
 */

// Imports
import MP = Modal.ModalPage;
import MNC = Modal.ModalNavigationController;
import {Modal} from "../../../lib/framework/ModalNavigation";
import ModalPageController = Modal.ModalPageController;

export class Controller extends ModalPageController {
    private $btnPageOne: JQuery;

    constructor(p: MP, $s: JQuery, nc: MNC) {
        super(p, $s, nc);

        let t = this;
        
        this.$btnPageOne = p.findById$("btnPageOne");
        this.$btnPageOne.on('click', function(){ t.btnPageOne_Click.call(t, this); });
    }

    get btnPageOneContent(): string { return this.$btnPageOne.html(); }
    set btnPageOneContent(v:string) { this.$btnPageOne.html(v); }

// Page Events
    public Shown(): void {
        this.print("Shown.");
    }

    public Load(): void {
        this.print("Load.");
    }

    public Leave(): void {
        this.print("Leave.");
    }

    public Close(): void {
        this.print("Close.");
    }

// Element Events
    private btnPageOne_Click(elem): void {
        // Change to page one
        this.nav.changePage("page1");
    }

// Functions
}
