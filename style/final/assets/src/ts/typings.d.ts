/************************************************************************
 * PROJECT:                Timetable-College
 *
 * FILE NAME:              typings.d.ts
 *
 * DESCRIPTION:
 *       Acts as a header file for typescript, creating definitions and
 *       adding any external library calls that can be accessed normally
 *       in javascript but not in typescript. So we wrap them in this
 *       file to allow for type hinting in a IDE and the language itself.
 *
 * NOTES:
 *       <Notes>
 *
 * AUTHOR:     Callum Carmicheal      START DATE:        21 Jan 2018
 *
 * NOTABLE CHANGES:
 *
 * REF NO   VERSION  DATE        WHO  DETAIL
 * SNN/NMM  $.NN.NN  DD.MM.YYYY  II   ------
 * *******  *******  **********  **   ************************************
 *                   21.01.2018  CC   File Created.
 *                   21.01.2018  CC   Added ArriveJS Definitions.
 *                   22.01.2018  CC   Added Custom JQuery Extensions.
 *                   24.01.2018  CC   Added isEmptyOrSpaced to Custom
 *                                    String Extensions.
 *                   24.01.2018  CC   Added Custom Window Extensions.
 *                   24.01.2018  CC   Added Timetable Global Types.
 */

// ***** Custom String Extensions ***** //
interface String {
    isEmptyOrSpaced(): boolean;
}

// ***** Custom JQuery Extensions ***** //
interface JQuery<TElement extends Node = HTMLElement> extends Iterable<TElement> {
    hasAttribute(AttributeName: string): boolean;
}

// ***** ARRIVEJS DEFINITIONS ***** //

type ArriveJS_ArriveCB =  (newElem?: HTMLElement|JQuery) => any;

interface JQuery<TElement extends Node = HTMLElement> extends Iterable<TElement> {
    arrive(selector:string, callback:ArriveJS_ArriveCB): JQuery;
    arrive(selector:string, options:object, callback:ArriveJS_ArriveCB);

    unbindArrive(selectorOrCallback?: string|Function);
    unbindArrive(selectorOrCallback?: string|Function, callback?: Function);

    unbindAllArrive();
}


