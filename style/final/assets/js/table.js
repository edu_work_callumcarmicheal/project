(function ($) {
    "use strict";
    var $tt_col = $('.timetable-col');
    $tt_col.on('mouseover', function () {
        var table1 = $(this).parent().parent().parent();
        var table2 = $(this).parent().parent();
        var row = $(this).parent();
        var column = $(this).data('column') + "";
        $(table2).find("." + column).addClass('hov-column');
        $(table1).find(".timetable-row.head ." + column).addClass('hov-column-head');
    });
    $tt_col.on('mouseout', function () {
        var table1 = $(this).parent().parent().parent();
        var table2 = $(this).parent().parent();
        var row = $(this).parent();
        var column = $(this).data('column') + "";
        $(table2).find("." + column).removeClass('hov-column');
        $(table1).find(".timetable-row.head ." + column).removeClass('hov-column-head');
    });
})(jQuery);
//# sourceMappingURL=table.js.map