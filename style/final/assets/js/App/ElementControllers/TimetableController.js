var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "../../lib/framework/Timetable"], function (require, exports, Timetable_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ElementController = Timetable_1.Lib.Framework.Timetable.ElementController;
    var ColumnType;
    (function (ColumnType) {
        ColumnType[ColumnType["COURSE"] = 1] = "COURSE";
        ColumnType[ColumnType["DAY"] = 2] = "DAY";
        ColumnType[ColumnType["TIME_START"] = 3] = "TIME_START";
        ColumnType[ColumnType["TIME_END"] = 4] = "TIME_END";
        ColumnType[ColumnType["ROOM"] = 5] = "ROOM";
        ColumnType[ColumnType["STAFF"] = 6] = "STAFF";
        ColumnType[ColumnType["CODE"] = 7] = "CODE";
        ColumnType[ColumnType["SUBJECT"] = 8] = "SUBJECT";
    })(ColumnType || (ColumnType = {}));
    var Controller = (function (_super) {
        __extends(Controller, _super);
        function Controller($s) {
            var _this = _super.call(this, $s) || this;
            _this.$cells = null;
            _this.mnc = null;
            _this.mnc = window["modal-timetable-editor"];
            return _this;
        }
        Controller.prototype.Load = function () {
            var t = this;
            console.log("こんにちは、私の", this.$self);
            this.$cells = this.$self.find('.timetable-col');
            this.$cells.dblclick(function (e) { t.cell_DoubleClick.call(t, this, e); });
        };
        Controller.prototype.cell_DoubleClick = function (element, event) {
            var colType = this.getColumnFor(element);
            if (colType == ColumnType.COURSE) {
                if (this.mnc.changePage("page1")) {
                    var page = this.mnc.getPageByName("page1");
                    var controller = page.getController();
                    console.log("mnc.show()");
                    this.mnc.show();
                    console.log("_tCurrentTime");
                    controller._tCurrentTime(false);
                    controller._tCurrentTime(false);
                    controller._tCurrentTime(false);
                    controller._tCurrentTime(false);
                }
            }
            else if (this.mnc.changePage("page3")) {
                var page = this.mnc.getPageByName("page3");
                var controller = page.getController();
                controller.btnPageOneContent = element.innerText;
                this.mnc.show();
            }
        };
        Controller.prototype.getColumnNumberFor = function (element) {
            return Number(element.getAttribute("data-column")[6]);
        };
        Controller.prototype.getColumnFor = function (element) {
            return this.getColumnNumberFor(element);
        };
        return Controller;
    }(ElementController));
    exports.Controller = Controller;
});
//# sourceMappingURL=TimetableController.js.map