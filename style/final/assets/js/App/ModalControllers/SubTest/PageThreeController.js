var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "../../../lib/framework/ModalNavigation"], function (require, exports, ModalNavigation_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ModalPageController = ModalNavigation_1.Modal.ModalPageController;
    var Controller = (function (_super) {
        __extends(Controller, _super);
        function Controller(p, $s, nc) {
            var _this = _super.call(this, p, $s, nc) || this;
            var t = _this;
            _this.$btnPageOne = p.findById$("btnPageOne");
            _this.$btnPageOne.on('click', function () { t.btnPageOne_Click.call(t, this); });
            return _this;
        }
        Object.defineProperty(Controller.prototype, "btnPageOneContent", {
            get: function () { return this.$btnPageOne.html(); },
            set: function (v) { this.$btnPageOne.html(v); },
            enumerable: true,
            configurable: true
        });
        Controller.prototype.Shown = function () {
            this.print("Shown.");
        };
        Controller.prototype.Load = function () {
            this.print("Load.");
        };
        Controller.prototype.Leave = function () {
            this.print("Leave.");
        };
        Controller.prototype.Close = function () {
            this.print("Close.");
        };
        Controller.prototype.btnPageOne_Click = function (elem) {
            this.nav.changePage("page1");
        };
        return Controller;
    }(ModalPageController));
    exports.Controller = Controller;
});
//# sourceMappingURL=PageThreeController.js.map