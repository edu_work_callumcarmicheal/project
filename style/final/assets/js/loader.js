define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Loader;
    (function (Loader) {
        function LoadFramework(cbLoaded) {
            console.log("[loader.ts] Loading framework and other files...");
            require([
                "./lib/framework/JQueryExtensions",
                "./lib/framework/Utility",
                "./table"
            ], function (_lib_framework_JQueryExtensions, _lib_framework_Utility, _table) {
                console.log("[loader.ts] -> Loaded files!");
                cbLoaded();
            });
        }
        Loader.LoadFramework = LoadFramework;
    })(Loader = exports.Loader || (exports.Loader = {}));
    var TestLoader;
    (function (TestLoader) {
        var Test = (function () {
            function Test() {
            }
            return Test;
        }());
        TestLoader.Test = Test;
    })(TestLoader = exports.TestLoader || (exports.TestLoader = {}));
});
//# sourceMappingURL=loader.js.map