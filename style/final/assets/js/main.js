define(["require", "exports", "./lib/framework/ModalNavigation", "./lib/framework/Timetable", "./loader"], function (require, exports, ModalNavigation_1, Timetable_1, loader_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ModalNavigationController = ModalNavigation_1.Modal.ModalNavigationController;
    var TimetableAppSettings = Timetable_1.Lib.Framework.Timetable.AppSettings;
    window.Timetable = TimetableAppSettings.CreateSettings(window.Timetable);
    console.info("[main.ts] Loading Framework...");
    loader_1.Loader.LoadFramework(function () {
        console.info("[main.ts] Loading Page...");
        $(pageLoad);
    });
    function loadPageController() {
        if (!window.Timetable.PageController.Enabled)
            return;
        console.info("[main.ts] => Finished loading Page Controller");
    }
    function loadElementControllers() {
        if (!window.Timetable.EnableElementControllers)
            return;
        $("[data-type='ElementController']").each(function (index, element) {
            var $element = $(element);
            if (!$element.hasAttribute("data-controller"))
                return;
            var controller = $element.attr('data-controller');
            if (controller.isEmptyOrSpaced())
                return;
            console.info("[main.ts]    Found Element Controller: ", controller);
            require(["App/ElementControllers/" + controller], function (c) {
                new c.Controller($element);
            }, function (err) {
                console.error("[main.ts]   Failed to load controller '" + controller + "'!", err);
            });
        });
        console.info("[main.ts] -> Finished loading Element Controllers");
    }
    function pageLoad() {
        console.log("[main.ts] Loading Page Controller...");
        loadPageController();
        console.log("[main.ts] Loading Element Controllers...");
        loadElementControllers();
        var navigation = new ModalNavigationController("#mdl-TimetableEditor");
        window["modal-timetable-editor"] = navigation;
        console.log("[main.ts] Loaded!");
    }
});
//# sourceMappingURL=main.js.map