String.prototype.isEmptyOrSpaced = function () {
    return this === null || this.match(/^ *$/) !== null;
};
//# sourceMappingURL=Utility.js.map