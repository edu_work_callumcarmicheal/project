var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "typescript-collections/dist/lib/Dictionary"], function (require, exports, Dictionary_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Lib;
    (function (Lib) {
        var Framework;
        (function (Framework) {
            var Timetable;
            (function (Timetable) {
                var AppSettings = (function (_super) {
                    __extends(AppSettings, _super);
                    function AppSettings(existingSettings, toStrFunction) {
                        var _this = _super.call(this, toStrFunction) || this;
                        _this._primitiveDefaults();
                        for (var key in existingSettings) {
                            var value = existingSettings[key];
                            _this.setValue(key, value);
                        }
                        _this._instanceDefaults();
                        return _this;
                    }
                    AppSettings.CreateSettings = function (existingSettings) {
                        var settings = new AppSettings(existingSettings);
                        return settings;
                    };
                    Object.defineProperty(AppSettings.prototype, "EnableElementControllers", {
                        get: function () { return this.getValue("EnableElementControllers"); },
                        set: function (v) { this.setValue("EnableElementControllers", v); },
                        enumerable: true,
                        configurable: true
                    });
                    Object.defineProperty(AppSettings.prototype, "PageController", {
                        get: function () { return this.getValue("PageController"); },
                        set: function (v) { this.setValue("PageController", v); },
                        enumerable: true,
                        configurable: true
                    });
                    AppSettings.prototype._primitiveDefaults = function () {
                        this.setValue("EnableElementControllers", true);
                    };
                    AppSettings.prototype._instanceDefaults = function () {
                        if (this.containsKey("PageController"))
                            this.setValue("PageController", new SettingPageController(this.getValue("PageController")));
                        else
                            this.setValue("PageController", new SettingPageController());
                    };
                    return AppSettings;
                }(Dictionary_1.default));
                Timetable.AppSettings = AppSettings;
                var PageController = (function () {
                    function PageController() {
                    }
                    PageController.prototype.OnReady = function () { };
                    ;
                    return PageController;
                }());
                Timetable.PageController = PageController;
                var ElementController = (function () {
                    function ElementController($s) {
                        this.$self = $s;
                        var t = this;
                        $(function () { t.Load.call(t); });
                    }
                    ElementController.prototype.getElem = function () {
                        return this.$self;
                    };
                    ElementController.prototype.getElemDom = function () {
                        return this.$self[0];
                    };
                    ElementController.prototype.Load = function () { };
                    return ElementController;
                }());
                Timetable.ElementController = ElementController;
                var SettingPageController = (function () {
                    function SettingPageController(ExistingSettings) {
                        this.Enabled = false;
                        this.Location = "";
                        this.Instance = null;
                        if (ExistingSettings == null)
                            return;
                        if (ExistingSettings.hasOwnProperty("Enabled"))
                            this.Enabled = ExistingSettings["Enabled"];
                        if (ExistingSettings.hasOwnProperty("Location"))
                            this.Location = ExistingSettings["Location"];
                        if (ExistingSettings.hasOwnProperty("Instance"))
                            this.Instance = ExistingSettings["Instance"];
                    }
                    return SettingPageController;
                }());
                Timetable.SettingPageController = SettingPageController;
            })(Timetable = Framework.Timetable || (Framework.Timetable = {}));
        })(Framework = Lib.Framework || (Lib.Framework = {}));
    })(Lib = exports.Lib || (exports.Lib = {}));
});
//# sourceMappingURL=Timetable.js.map