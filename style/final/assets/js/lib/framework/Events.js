define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Lib;
    (function (Lib) {
        var Framework;
        (function (Framework) {
            var Event = (function () {
                function Event() {
                    this.handlers = [];
                }
                Event.prototype.on = function (handler) {
                    this.handlers.push(handler);
                };
                Event.prototype.off = function (handler) {
                    this.handlers = this.handlers.filter(function (h) { return h !== handler; });
                };
                Event.prototype.trigger = function (data) {
                    this.handlers.slice(0).forEach(function (h) { return h(data); });
                };
                Event.prototype.expose = function () {
                    return this;
                };
                return Event;
            }());
            Framework.Event = Event;
        })(Framework = Lib.Framework || (Lib.Framework = {}));
    })(Lib = exports.Lib || (exports.Lib = {}));
});
//# sourceMappingURL=Events.js.map