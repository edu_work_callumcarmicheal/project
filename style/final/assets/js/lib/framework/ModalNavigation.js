define(["require", "exports", "typescript-collections/dist/lib/Dictionary"], function (require, exports, Dictionary_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Modal;
    (function (Modal) {
        var ModalNavigationController = (function () {
            function ModalNavigationController(selector) {
                this.c_mdlHeader = "mdl-header";
                this.c_mdlBody = "mdl-body";
                this.c_mdlFooter = "mdl-footer";
                this.d_mdlHeaderTitle = "mdl-header-title";
                this.t_Page = "page";
                this.t_Container = "container";
                this.t_DisplayText = "display";
                this.t_AnchorLink = "mdl-link";
                this.s_Pages = "> div[data-type='" + this.t_Page + "']";
                this.s_ActivePage = "> div[data-type='" + this.t_Page + "'][data-active=\"true\"]";
                this.s_LinkNav = "a[href='#'][data-type='" + this.t_AnchorLink + "']";
                this.$modal = $(selector);
                this.dPages = new Dictionary_1.default();
                this._populatePages();
                this._handleNewPages();
                this._bindings();
            }
            ModalNavigationController.prototype._bindings = function () {
                var mnc = this;
                this.$modal.find(this.s_LinkNav).click(function () {
                    var $this = $(this);
                    var target = $this.attr('data-page');
                    var test = "Hello";
                    if (target.isEmptyOrSpaced())
                        return;
                    mnc.changePage(target);
                });
                this.$modal.on('show.bs.modal', function (event) {
                    mnc.getActivePage().Load();
                });
                this.$modal.on('hide.bs.modal', function (event) {
                    mnc.getActivePage().Close();
                });
            };
            ModalNavigationController.prototype._populatePages = function () {
                var $container = this.getContainerById$(this.c_mdlBody);
                var $pages = $container.find(this.s_Pages);
                var activePage = "";
                var setActivePage = false;
                for (var i = 0; i < $pages.length; i++) {
                    var $e = $($pages[i]);
                    var mp = new ModalPage($e, this);
                    if (this.dPages.containsKey(mp.getName()))
                        console.warn("[ModalNavigation (" + this.getId() + ")] Possibly duplicate page (same name). " + mp.getName());
                    this.dPages.setValue(mp.getName(), mp);
                    if (mp.isActive()) {
                        if (setActivePage) {
                            console.warn("[ModalNavigation (" + this.getId() + ")] There is multiple active pages, Current=" + activePage + ", Collision=" + mp.getName());
                        }
                        else {
                            setActivePage = true;
                            activePage = mp.getName();
                        }
                    }
                }
                if (setActivePage) {
                    this.activePage = activePage;
                }
                else {
                    console.warn("[ModalNavigation (${this.getId()})] No default page set, before dialog is shown a page is required to be set.");
                }
            };
            ModalNavigationController.prototype._handleNewPages = function () {
                var $container = this.getContainerById$(this.c_mdlBody);
                $container.arrive(this.s_Pages, function (newElem) {
                    var $newElem = $(newElem);
                    var mp = new ModalPage($newElem, this);
                    if (this.dPages.containsKey(mp.getName()))
                        console.warn("[ModalNavigation (" + this.getId() + ")] Possibly duplicate page (same name). " + mp.getName());
                    this.dPages.setValue(mp.getName(), mp);
                });
            };
            ModalNavigationController.prototype.changePage = function (newPage) {
                var strName = newPage instanceof ModalPage ? newPage.getName() : newPage;
                if (this.dPages.containsKey(strName)) {
                    this.activePage = strName;
                    var $cActivePage = this.getContainerById$(this.c_mdlBody).find(this.s_ActivePage);
                    if ($cActivePage != null) {
                        var cActivePageName = $cActivePage.attr("data-page");
                        $cActivePage.addClass('hidden').removeAttr('data-active');
                        if (this.dPages.containsKey(cActivePageName)) {
                            var currentPage = this.dPages.getValue(cActivePageName);
                            currentPage.Leave();
                        }
                    }
                    this.getPageByName(this.activePage).getElem().removeClass("hidden").attr('data-active', "true");
                    this.dPages.getValue(this.activePage).Shown();
                    return true;
                }
                return false;
            };
            ModalNavigationController.prototype.getContainerById$ = function (id) {
                return this.$modal.find("div[data-id='" + id + "'][data-type='container']");
            };
            ModalNavigationController.prototype.getContainerById = function (id) {
                return this.getContainerById$(id)[0];
            };
            ModalNavigationController.prototype.getElem = function () {
                return this.$modal;
            };
            ModalNavigationController.prototype.getElemDom = function () {
                return this.$modal[0];
            };
            ModalNavigationController.prototype.getPageByName = function (name) {
                if (this.dPages.containsKey(name))
                    return this.dPages.getValue(name);
                return null;
            };
            ModalNavigationController.prototype.getActivePage = function () {
                if (this.dPages.containsKey(this.activePage))
                    return this.dPages.getValue(this.activePage);
                console.error("[ModalNavigation (" + this.getId() + ")] No activePage: " + this.activePage);
                return null;
            };
            ModalNavigationController.prototype.getActivePageName = function () {
                return this.activePage;
            };
            ModalNavigationController.prototype.getId = function () {
                return this.$modal.attr("id");
            };
            ModalNavigationController.prototype.rebindAll = function () {
                return this._bindings();
            };
            ModalNavigationController.prototype.show = function () { this.$modal.modal('show'); };
            ModalNavigationController.prototype.hide = function () { this.$modal.modal('hide'); };
            ModalNavigationController.prototype.saveButton_SetVisibility = function (state) {
                if (state) {
                    this.getContainerById$(this.c_mdlFooter).find("button[data-id='save']").addClass('hidden');
                }
                else {
                    this.getContainerById$(this.c_mdlFooter).find("button[data-id='save']").removeClass('hidden');
                }
            };
            ModalNavigationController.prototype.getShared = function () {
                return this.dSharedVars;
            };
            return ModalNavigationController;
        }());
        Modal.ModalNavigationController = ModalNavigationController;
        var ModalPage = (function () {
            function ModalPage(self, parent) {
                this.controller = null;
                this.parent = parent;
                this.$self = self;
                this._setupController();
            }
            ModalPage.prototype._setupController = function () {
                if (!this.$self.hasAttribute("data-controller"))
                    return;
                var strController = this.$self.attr("data-controller");
                var page = this;
                if (strController.isEmptyOrSpaced())
                    return;
                require(["App/ModalControllers/" + strController], function (c) {
                    page.controller = new c.Controller(page, page.getElem(), page.parent);
                }, function (err) {
                    console.error("[ModalPage(" + page.getName() + ")] Failed to load controller!", err);
                });
            };
            ModalPage.prototype.getElem = function () {
                return this.$self;
            };
            ModalPage.prototype.getElemDom = function () {
                return this.getElem()[0];
            };
            ModalPage.prototype.findById = function (id) {
                return this.findById$(id)[0];
            };
            ModalPage.prototype.findById$ = function (id) {
                return this.$self.find("[data-id='" + id + "']");
            };
            ModalPage.prototype.getName = function () {
                return this.$self.attr("data-page");
            };
            ModalPage.prototype.isActive = function () {
                return this.$self.attr("data-active") == "true";
            };
            ModalPage.prototype.Save = function () {
                if (this.controller == null)
                    return false;
                return this.controller.Save();
            };
            ModalPage.prototype.Shown = function () {
                if (this.controller == null)
                    return;
                this.controller.Shown();
            };
            ModalPage.prototype.Load = function () {
                if (this.controller == null)
                    return;
                this.controller.Load();
            };
            ModalPage.prototype.Leave = function () {
                if (this.controller == null)
                    return;
                this.controller.Leave();
            };
            ModalPage.prototype.Close = function () {
                if (this.controller == null)
                    return;
                this.controller.Close();
            };
            ModalPage.prototype.hasController = function () {
                return this.controller != null;
            };
            ModalPage.prototype.getController = function () {
                return this.controller;
            };
            return ModalPage;
        }());
        Modal.ModalPage = ModalPage;
        var ModalPageController = (function () {
            function ModalPageController(page, $self, navController) {
                this.Page = page;
                this.$self = $self;
                this.nav = navController;
            }
            ModalPageController.prototype.Save = function () { return false; };
            ;
            ModalPageController.prototype.Load = function () { };
            ;
            ModalPageController.prototype.Close = function () { };
            ;
            ModalPageController.prototype.Shown = function () { };
            ;
            ModalPageController.prototype.Leave = function () { };
            ;
            ModalPageController.prototype.print = function (str) {
                console.info("[" + this.Page.getName() + "] " + str);
            };
            return ModalPageController;
        }());
        Modal.ModalPageController = ModalPageController;
    })(Modal = exports.Modal || (exports.Modal = {}));
});
//# sourceMappingURL=ModalNavigation.js.map