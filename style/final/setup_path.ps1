# SET NODE_PATH=%%~dp0/assets/node_modules

$cwd = (Get-Item -Path ".\" -Verbose).FullName;
$env:NODE_PATH = "$cwd\\assets\\node_modules";